#include "text.h"

Text::Text() :
  created_(false),
  font_(Font::DEFAULT),
  bounds_(sf::FloatRect(0.0f, 0.0f, 0.0f, 0.0f)),
  texture_(nullptr),
  position_(sf::Vector2f(0.0f, 0.0f)),
  string_(""),
  scale_(1.0f),
  color_(sf::Color(0, 0, 0))
{
}

Text::Text(sf::Vector2f pos, const std::string str, Font font, sf::Texture &texture, float scale, sf::Color color) :
  created_(false),
  font_(font),
  bounds_(sf::FloatRect(pos.x, pos.y, 0.0f, 0.0f)),
  texture_(&texture),
  position_(pos),
  string_(""),
  scale_(scale),
  color_(color)
{
  setString(str);
}

Text::~Text() {
}

const sf::Vector2f Text::getPos() const {
  return sf::Vector2f(bounds_.left, bounds_.top);
}

const sf::FloatRect &Text::getBounds() const {
  return bounds_;
}

const std::string &Text::getString() const {
  return string_;
}

const float Text::getWidth() const {
  return bounds_.width;
}

const float Text::getHeight() const {
  return bounds_.height;
}

const int Text::getAlpha() const {
  return color_.a;
}

void Text::setPos(const sf::Vector2f &pos) {
  position_ = pos;

  bounds_.left = position_.x;
  bounds_.top = position_.y;
  sf::Vector2f posOffset(0.0f, 0.0f);
  for (std::size_t i = 0; i < chars_.size(); ++i) {
    chars_[i].setPosition(pos + posOffset);
    posOffset.x += chars_[i].getGlobalBounds().width;
  }
}

void Text::setString(const std::string str) {
  const std::size_t stringLength = str.length();

  if (stringLength < 1) {
    return;
  }

  string_ = str;
  created_ = false;
  chars_.clear();

  sf::Vector2f posOffset(0.0f, 0.0f);
  bounds_.width = 0.0f;
  float greatestHeight = 0.0f;
  
  for (std::size_t i = 0; i < stringLength; ++i) {

    Char curChar = charToEnum(string_.at(i));
    if (curChar == Char::CHAR_UNKNOWN) {
      continue;
    }

    sf::IntRect textureRect(0, 0, 0, 0);

    switch (font_) {

      case Font::DEFAULT:
      default:
        {
          switch (curChar) {
            case Char::CHAR_A: textureRect = sf::IntRect(0, 0, 50, 50); break;
            case Char::CHAR_B: textureRect = sf::IntRect(50, 0, 50, 50); break;
            case Char::CHAR_C: textureRect = sf::IntRect(100, 0, 40, 50); break;
            case Char::CHAR_D: textureRect = sf::IntRect(140, 0, 50, 50); break;
            case Char::CHAR_E: textureRect = sf::IntRect(190, 0, 50, 50); break;
            case Char::CHAR_F: textureRect = sf::IntRect(240, 0, 40, 50); break;
            case Char::CHAR_G: textureRect = sf::IntRect(280, 0, 50, 70); break;
            case Char::CHAR_H: textureRect = sf::IntRect(330, 0, 50, 50); break;
            case Char::CHAR_I: textureRect = sf::IntRect(380, 0, 20, 50); break;
            case Char::CHAR_J: textureRect = sf::IntRect(400, 0, 30, 70); break;
            case Char::CHAR_K: textureRect = sf::IntRect(430, 0, 50, 50); break;
            case Char::CHAR_L: textureRect = sf::IntRect(480, 0, 20, 50); break;
            case Char::CHAR_M: textureRect = sf::IntRect(500, 0, 60, 50); break;
            case Char::CHAR_N: textureRect = sf::IntRect(560, 0, 50, 50); break;
            case Char::CHAR_O: textureRect = sf::IntRect(610, 0, 50, 50); break;
            case Char::CHAR_P: textureRect = sf::IntRect(660, 0, 50, 70); break;
            case Char::CHAR_Q: textureRect = sf::IntRect(710, 0, 50, 70); break;
            case Char::CHAR_R: textureRect = sf::IntRect(760, 0, 40, 50); break;
            case Char::CHAR_S: textureRect = sf::IntRect(800, 0, 50, 50); break;
            case Char::CHAR_T: textureRect = sf::IntRect(850, 0, 40, 50); break;
            case Char::CHAR_U: textureRect = sf::IntRect(890, 0, 50, 50); break;
            case Char::CHAR_V: textureRect = sf::IntRect(940, 0, 50, 50); break;
            case Char::CHAR_W: textureRect = sf::IntRect(990, 0, 60, 50); break;
            case Char::CHAR_X: textureRect = sf::IntRect(1050, 0, 40, 50); break;
            case Char::CHAR_Y: textureRect = sf::IntRect(1090, 0, 50, 70); break;
            case Char::CHAR_Z: textureRect = sf::IntRect(1140, 0, 40, 50); break;

            case Char::CHAR_A_CAPITAL: textureRect = sf::IntRect(0, 80, 50, 50); break;
            case Char::CHAR_B_CAPITAL: textureRect = sf::IntRect(50, 80, 50, 50); break;
            case Char::CHAR_C_CAPITAL: textureRect = sf::IntRect(100, 80, 40, 50); break;
            case Char::CHAR_D_CAPITAL: textureRect = sf::IntRect(140, 80, 50, 50); break;
            case Char::CHAR_E_CAPITAL: textureRect = sf::IntRect(190, 80, 40, 50); break;
            case Char::CHAR_F_CAPITAL: textureRect = sf::IntRect(230, 80, 40, 50); break;
            case Char::CHAR_G_CAPITAL: textureRect = sf::IntRect(270, 80, 50, 50); break;
            case Char::CHAR_H_CAPITAL: textureRect = sf::IntRect(320, 80, 50, 50); break;
            case Char::CHAR_I_CAPITAL: textureRect = sf::IntRect(370, 80, 40, 50); break;
            case Char::CHAR_J_CAPITAL: textureRect = sf::IntRect(410, 80, 50, 50); break;
            case Char::CHAR_K_CAPITAL: textureRect = sf::IntRect(460, 80, 50, 50); break;
            case Char::CHAR_L_CAPITAL: textureRect = sf::IntRect(510, 80, 40, 50); break;
            case Char::CHAR_M_CAPITAL: textureRect = sf::IntRect(550, 80, 60, 50); break;
            case Char::CHAR_N_CAPITAL: textureRect = sf::IntRect(610, 80, 50, 50); break;
            case Char::CHAR_O_CAPITAL: textureRect = sf::IntRect(660, 80, 50, 50); break;
            case Char::CHAR_P_CAPITAL: textureRect = sf::IntRect(710, 80, 50, 50); break;
            case Char::CHAR_Q_CAPITAL: textureRect = sf::IntRect(760, 80, 50, 60); break;
            case Char::CHAR_R_CAPITAL: textureRect = sf::IntRect(810, 80, 50, 50); break;
            case Char::CHAR_S_CAPITAL: textureRect = sf::IntRect(860, 80, 50, 50); break;
            case Char::CHAR_T_CAPITAL: textureRect = sf::IntRect(910, 80, 40, 50); break;
            case Char::CHAR_U_CAPITAL: textureRect = sf::IntRect(950, 80, 50, 50); break;
            case Char::CHAR_V_CAPITAL: textureRect = sf::IntRect(1000, 80, 50, 50); break;
            case Char::CHAR_W_CAPITAL: textureRect = sf::IntRect(1050, 80, 60, 50); break;
            case Char::CHAR_X_CAPITAL: textureRect = sf::IntRect(1110, 80, 50, 50); break;
            case Char::CHAR_Y_CAPITAL: textureRect = sf::IntRect(1160, 80, 50, 50); break;
            case Char::CHAR_Z_CAPITAL: textureRect = sf::IntRect(1210, 80, 40, 50); break;

            case Char::CHAR_1: textureRect = sf::IntRect(0, 150, 50, 50); break;
            case Char::CHAR_2: textureRect = sf::IntRect(50, 150, 50, 50); break;
            case Char::CHAR_3: textureRect = sf::IntRect(100, 150, 50, 50); break;
            case Char::CHAR_4: textureRect = sf::IntRect(150, 150, 50, 50); break;
            case Char::CHAR_5: textureRect = sf::IntRect(200, 150, 50, 50); break;
            case Char::CHAR_6: textureRect = sf::IntRect(250, 150, 50, 50); break;
            case Char::CHAR_7: textureRect = sf::IntRect(300, 150, 50, 50); break;
            case Char::CHAR_8: textureRect = sf::IntRect(350, 150, 50, 50); break;
            case Char::CHAR_9: textureRect = sf::IntRect(400, 150, 50, 50); break;
            case Char::CHAR_0: textureRect = sf::IntRect(450, 150, 50, 50); break;

            case Char::CHAR_SPACE: textureRect = sf::IntRect(0, 210, 40, 50); break;
            case Char::CHAR_PLUS: textureRect = sf::IntRect(40, 210, 40, 50); break;
            case Char::CHAR_MINUS: textureRect = sf::IntRect(80, 210, 40, 50); break;
            case Char::CHAR_MULTIPLY: textureRect = sf::IntRect(120, 210, 40, 50); break;
            case Char::CHAR_DIVIDE: textureRect = sf::IntRect(160, 210, 60, 50); break;
            case Char::CHAR_EQUALS: textureRect = sf::IntRect(220, 210, 40, 50); break;
            case Char::CHAR_NUMBER_SIGN: textureRect = sf::IntRect(260, 210, 60, 50); break;
            case Char::CHAR_AMPERSAND: textureRect = sf::IntRect(320, 210, 40, 50); break;
            case Char::CHAR_UNDERSCORE: textureRect = sf::IntRect(380, 210, 50, 50); break;
            case Char::CHAR_PARANTHESIS_LEFT: textureRect = sf::IntRect(430, 210, 30, 50); break;
            case Char::CHAR_PARANTHESIS_RIGHT: textureRect = sf::IntRect(460, 210, 30, 50); break;
            case Char::CHAR_BRACKET_LEFT: textureRect = sf::IntRect(490, 210, 30, 50); break;
            case Char::CHAR_BRACKET_RIGHT: textureRect = sf::IntRect(520, 210, 30, 50); break;
            case Char::CHAR_BIG_BRACKET_LEFT: textureRect = sf::IntRect(550, 210, 40, 50); break;
            case Char::CHAR_BIG_BRACKET_RIGHT: textureRect = sf::IntRect(590, 210, 40, 50); break;
            case Char::CHAR_LESS_THAN: textureRect = sf::IntRect(630, 210, 40, 50); break;
            case Char::CHAR_GREATER_THAN: textureRect = sf::IntRect(670, 210, 40, 50); break;
            case Char::CHAR_COMMA: textureRect = sf::IntRect(710, 210, 30, 60); break;
            case Char::CHAR_FULL_STOP: textureRect = sf::IntRect(740, 210, 20, 50); break;
            case Char::CHAR_SEMICOLON: textureRect = sf::IntRect(760, 210, 20, 50); break;
            case Char::CHAR_COLON: textureRect = sf::IntRect(780, 210, 20, 50); break;
            case Char::CHAR_QUESTION_MARK: textureRect = sf::IntRect(800, 210, 50, 50); break;
            case Char::CHAR_EXCLAMATION_MARK: textureRect = sf::IntRect(850, 210, 20, 50); break;
            case Char::CHAR_APOSTROPHE: textureRect = sf::IntRect(870, 210, 20, 50); break;
            case Char::CHAR_AT: textureRect = sf::IntRect(890, 210, 60, 50); break;
            case Char::CHAR_INFINITY: textureRect = sf::IntRect(950, 210, 60, 50); break;

            case Char::CHAR_UNKNOWN:
            default:
              continue;
              break;
          }
        }
        break;

    }

    sf::Sprite newSprite(*texture_, textureRect);
    newSprite.setPosition(position_ + posOffset);
    newSprite.setScale(scale_, scale_);
    newSprite.setColor(color_);
      
    chars_.push_back(newSprite);

    float addWidth = newSprite.getGlobalBounds().width;
    posOffset.x += addWidth;
    bounds_.width += addWidth;

    if (newSprite.getGlobalBounds().height > greatestHeight) {
      greatestHeight = newSprite.getGlobalBounds().height;
    }

  }

  bounds_.height = greatestHeight;

  created_ = true;
}

void Text::setColor(const sf::Color &color) {
  color_ = color;
  for (std::size_t i = 0; i < chars_.size(); ++i) {
    chars_[i].setColor(color_);
  }
}

void Text::setAlpha(const int &alpha) {
  if ((alpha >= 0) && (alpha <= 255)) {
    color_.a = alpha;
    for (std::size_t i = 0; i < chars_.size(); ++i) {
      chars_[i].setColor(color_);
    }
  }
}

void Text::setScale(const float scale) {
  if (scale >= 0.0f) {
    for (std::size_t i = 0; i < chars_.size(); ++i) {
      chars_[i].setScale(scale, scale);
    }
  }
}

void Text::draw(sf::RenderTarget &renderTarget, sf::RenderStates renderStates) const {
  if (created_) {
    for (std::size_t i = 0; i < chars_.size(); ++i) {
      renderTarget.draw(chars_[i]);
    }
  }
}

Text::Char Text::charToEnum(const char myChar) const {
  if (myChar == 'a') return Char::CHAR_A;
  if (myChar == 'b') return Char::CHAR_B;
  if (myChar == 'c') return Char::CHAR_C;
  if (myChar == 'd') return Char::CHAR_D;
  if (myChar == 'e') return Char::CHAR_E;
  if (myChar == 'f') return Char::CHAR_F;
  if (myChar == 'g') return Char::CHAR_G;
  if (myChar == 'h') return Char::CHAR_H;
  if (myChar == 'i') return Char::CHAR_I;
  if (myChar == 'j') return Char::CHAR_J;
  if (myChar == 'k') return Char::CHAR_K;
  if (myChar == 'l') return Char::CHAR_L;
  if (myChar == 'm') return Char::CHAR_M;
  if (myChar == 'n') return Char::CHAR_N;
  if (myChar == 'o') return Char::CHAR_O;
  if (myChar == 'p') return Char::CHAR_P;
  if (myChar == 'q') return Char::CHAR_Q;
  if (myChar == 'r') return Char::CHAR_R;
  if (myChar == 's') return Char::CHAR_S;
  if (myChar == 't') return Char::CHAR_T;
  if (myChar == 'u') return Char::CHAR_U;
  if (myChar == 'v') return Char::CHAR_V;
  if (myChar == 'w') return Char::CHAR_W;
  if (myChar == 'x') return Char::CHAR_X;
  if (myChar == 'y') return Char::CHAR_Y;
  if (myChar == 'z') return Char::CHAR_Z;

  if (myChar == 'A') return Char::CHAR_A_CAPITAL;
  if (myChar == 'B') return Char::CHAR_B_CAPITAL;
  if (myChar == 'C') return Char::CHAR_C_CAPITAL;
  if (myChar == 'D') return Char::CHAR_D_CAPITAL;
  if (myChar == 'E') return Char::CHAR_E_CAPITAL;
  if (myChar == 'F') return Char::CHAR_F_CAPITAL;
  if (myChar == 'G') return Char::CHAR_G_CAPITAL;
  if (myChar == 'H') return Char::CHAR_H_CAPITAL;
  if (myChar == 'I') return Char::CHAR_I_CAPITAL;
  if (myChar == 'J') return Char::CHAR_J_CAPITAL;
  if (myChar == 'K') return Char::CHAR_K_CAPITAL;
  if (myChar == 'L') return Char::CHAR_L_CAPITAL;
  if (myChar == 'M') return Char::CHAR_M_CAPITAL;
  if (myChar == 'N') return Char::CHAR_N_CAPITAL;
  if (myChar == 'O') return Char::CHAR_O_CAPITAL;
  if (myChar == 'P') return Char::CHAR_P_CAPITAL;
  if (myChar == 'Q') return Char::CHAR_Q_CAPITAL;
  if (myChar == 'R') return Char::CHAR_R_CAPITAL;
  if (myChar == 'S') return Char::CHAR_S_CAPITAL;
  if (myChar == 'T') return Char::CHAR_T_CAPITAL;
  if (myChar == 'U') return Char::CHAR_U_CAPITAL;
  if (myChar == 'V') return Char::CHAR_V_CAPITAL;
  if (myChar == 'W') return Char::CHAR_W_CAPITAL;
  if (myChar == 'X') return Char::CHAR_X_CAPITAL;
  if (myChar == 'Y') return Char::CHAR_Y_CAPITAL;
  if (myChar == 'Z') return Char::CHAR_Z_CAPITAL;

  if (myChar == '1') return Char::CHAR_1;
  if (myChar == '2') return Char::CHAR_2;
  if (myChar == '3') return Char::CHAR_3;
  if (myChar == '4') return Char::CHAR_4;
  if (myChar == '5') return Char::CHAR_5;
  if (myChar == '6') return Char::CHAR_6;
  if (myChar == '7') return Char::CHAR_7;
  if (myChar == '8') return Char::CHAR_8;
  if (myChar == '9') return Char::CHAR_9;
  if (myChar == '0') return Char::CHAR_0;

  if (myChar == ' ') return Char::CHAR_SPACE;
  if (myChar == '+') return Char::CHAR_PLUS;
  if (myChar == '-') return Char::CHAR_MINUS;
  if (myChar == '*') return Char::CHAR_MULTIPLY;
  if (myChar == '/') return Char::CHAR_DIVIDE;
  if (myChar == '=') return Char::CHAR_EQUALS;
  if (myChar == '#') return Char::CHAR_NUMBER_SIGN;
  if (myChar == '&') return Char::CHAR_AMPERSAND;
  if (myChar == '_') return Char::CHAR_UNDERSCORE;
  if (myChar == '(') return Char::CHAR_PARANTHESIS_LEFT;
  if (myChar == ')') return Char::CHAR_PARANTHESIS_RIGHT;
  if (myChar == '[') return Char::CHAR_BRACKET_LEFT;
  if (myChar == ']') return Char::CHAR_BRACKET_RIGHT;
  if (myChar == '{') return Char::CHAR_BIG_BRACKET_LEFT;
  if (myChar == '}') return Char::CHAR_BIG_BRACKET_RIGHT;
  if (myChar == '<') return Char::CHAR_LESS_THAN;
  if (myChar == '>') return Char::CHAR_GREATER_THAN;
  if (myChar == ',') return Char::CHAR_COMMA;
  if (myChar == '.') return Char::CHAR_FULL_STOP;
  if (myChar == ';') return Char::CHAR_SEMICOLON;
  if (myChar == ':') return Char::CHAR_COLON;
  if (myChar == '?') return Char::CHAR_QUESTION_MARK;
  if (myChar == '!') return Char::CHAR_EXCLAMATION_MARK;
  if (myChar == '\'') return Char::CHAR_APOSTROPHE;
  if (myChar == '@') return Char::CHAR_AT;
  if (myChar == '%') return Char::CHAR_INFINITY; // !

  return Char::CHAR_UNKNOWN;
}