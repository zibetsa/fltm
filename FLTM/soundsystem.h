#ifndef SOUNDSYSTEM_H_INCLUDED
#define SOUNDSYSTEM_H_INCLUDED

#include <SFML/Audio.hpp>

#include "resources.h"

class SoundSystem {
 public:
  SoundSystem();
  virtual ~SoundSystem();
  void init(Resources *resources);

  void setSfxEnabled(const bool status);
  void setSfxVolume(const unsigned int volume);
  void playSound(const Resources::Sound sound);
  void pauseSfx();
  void resumeSfx();
  void stopSfx();

 private:
  Resources *resources_;

  bool sfxEnabled_;
  unsigned int maximumSounds_;
  std::vector<Resources::Sound> soundIdentifiers_;
  std::vector<sf::Sound> sounds_;
  unsigned int sfxVolume_;
  int lastSoundPlayed_;

};

#endif