#ifndef MAINCLASS_H_INCLUDED
#define MAINCLASS_H_INCLUDED

#include <memory>

#include <SFML/Graphics.hpp>

#include "resources.h"
#include "settings.h"
#include "soundsystem.h"

class MainClass {
 public:
  MainClass();
  virtual ~MainClass();

  int operator()();

 private:
  std::unique_ptr<sf::RenderWindow> window_;

  Resources resources_;
  Settings settings_;
  SoundSystem soundSystem_;

};

#endif