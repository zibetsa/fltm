#include "soundsystem.h"
#include "utilities.h"

SoundSystem::SoundSystem() : 
  resources_(nullptr),

  sfxEnabled_(true),
  maximumSounds_(30),
  sfxVolume_(25),
  lastSoundPlayed_(0)
{
  soundIdentifiers_.resize(maximumSounds_, Resources::Sound::DUMMY);
  sounds_.resize(maximumSounds_);
  
  setSfxVolume(sfxVolume_);
}

SoundSystem::~SoundSystem() {
}

void SoundSystem::init(Resources *resources) {
  resources_ = resources;
}

void SoundSystem::setSfxEnabled(const bool status) {
  sfxEnabled_ = status;
}

void SoundSystem::setSfxVolume(const unsigned int volume) {
  sfxVolume_ = static_cast<unsigned int>(utils::clamp(static_cast<int>(volume), 0, 100));

  for (std::size_t i = 0; i < maximumSounds_; ++i) {
    sounds_[i].setVolume(static_cast<float>(sfxVolume_));
  }
}

void SoundSystem::playSound(const Resources::Sound sound) {
  if (!sfxEnabled_) {
    return;
  }

  if (lastSoundPlayed_ == maximumSounds_) {
    lastSoundPlayed_ = 0;
  }

  sf::Sound &s = sounds_[lastSoundPlayed_];

  if (s.getStatus() == sf::Sound::Playing) {
    s.stop();
  }
  soundIdentifiers_[lastSoundPlayed_] = sound;
  s.setBuffer(resources_->getSoundBuffer(sound));
  s.play();

  ++lastSoundPlayed_;
}

void SoundSystem::pauseSfx() {
  for (std::size_t i = 0; i < sounds_.size(); ++i) {
    if (sounds_[i].getStatus() == sf::Sound::Status::Playing) {
      if (soundIdentifiers_[i] == Resources::Sound::PAUSE) {
        continue;
      }
      sounds_[i].pause();
    }
  }
}

void SoundSystem::resumeSfx() {
  for (std::size_t i = 0; i < sounds_.size(); ++i) {
    if (sounds_[i].getStatus() == sf::Sound::Status::Paused) {
      sounds_[i].play();
    }
  }
}

void SoundSystem::stopSfx() {
  for (std::size_t i = 0; i < sounds_.size(); ++i) {
    sounds_[i].stop();
  }
}