#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED

#include <string>

#include <SFML/Graphics.hpp>

namespace utils {

const float getPi();

const float getTileSize();

const int getRandomInt(int min, int max);

const float getRandomFloat(float min, float max);

const float degreesToRadians(const float angleDegrees);

const float radiansToDegrees(const float angleRadians);

template <class T>
T clamp(T value, T min, T max) {
  return (value < min) ? min : ((value > max) ? max : value);
}

sf::RectangleShape createRect(const sf::Vector2f pos, const sf::Vector2f dim, const sf::Color color, const float outline, const sf::Color outlineColor);

sf::RectangleShape createTexturedRect(const sf::Vector2f pos, const sf::Vector2f dim, const sf::Color color, const sf::Texture &texture, const sf::IntRect textureRect);

void spring(float &value, float &velocity, const float targetValue, const float dampingRatio, const float angularFrequency, const float timeStep);

const bool getChance(const int number);

}

#endif