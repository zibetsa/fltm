#ifndef RES_SOUNDS_H_INCLUDED
#define RES_SOUNDS_H_INCLUDED

#include "res_sound_dummy.h"

#include "res_sound_answer_correct.h"
#include "res_sound_answer_incorrect.h"
#include "res_sound_finished.h"
#include "res_sound_menu_confirm.h"
#include "res_sound_menu_select.h"
#include "res_sound_pause.h"

#endif