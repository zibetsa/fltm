#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include <memory>

#include <SFML/Graphics.hpp>

#include "camera.h"
#include "resources.h"
#include "settings.h"
#include "soundsystem.h"
#include "text.h"
#include "tile.h"

class Game {
 public:
  Game();
  Game(sf::RenderWindow *window, Resources *resources, Settings *settings, SoundSystem *soundSystem);
  virtual ~Game();

  void processEvents();
  void update(sf::Time dt);
  void render();

 private:
  enum class State {
    TITLE,
    HELP,
    ABOUT,
    RUNNING,
    PAUSED,
    FINISHED
  };

  enum class SelectedGroup {
    UP,
    RIGHT,
    DOWN,
    LEFT
  };

  enum class EventType {
    NONE,
    WRONG_ANSWER
  };

  void create();
  void render_game();
  void render_game_tiles(sf::RenderTarget &renderTarget);
  void render_game_info(sf::RenderTarget &renderTarget);
  void render_menu();
  void pauseGame();
  void resumeGame();
  void restartGame();
  void quitGame();
  void initTitle();
  void initGame();
  void updateGameInfo(const bool updDifficulty, const bool updRounds, const bool updAnswers, const bool updTimer);
  void generateRound();
  void genNewRoundTime();
  void pressedButton(const SelectedGroup selectedGroup, const bool forceWrongAnswer = false);
  void flashBackground(const EventType eventType);
  void deactivateTiles(const unsigned int groupIndex);
  void menu_select();
  void menu_processAction(const std::size_t index, const bool rightButtonSide = true);
  void menu_title_difficulty(const bool increase);
  void menu_title_rounds(const bool increase);
  void menu_title_timer(const bool status);
  void menu_title_updateTextsPos();
  void menu_settings_fps(const bool status);
  void menu_settings_fpsValue(const bool increase);
  void menu_settings_vsync(const bool status);
  void menu_settings_sfx(const bool status);
  void menu_settings_sfxVol(const bool increase);
  void menu_settings_updateTextsPos();
  void menu_settings_applyVideo();
  void exitProgram();

  sf::RenderWindow *window_;
  Resources *resources_;
  Settings *settings_;
  SoundSystem *soundSystem_;
  State state_;
  Camera camera_;
  sf::Vector2f mousePos_;

  bool menuSelectionRectDraw_;
  unsigned int menuSelectionIndex_;
  bool menu_settings_showApplyButton_;

  std::vector<bool> shakeCamera_;

  unsigned int game_currentRound_;
  unsigned int game_numAnswersCorrect_;
  unsigned int game_numAnswersWrong_;
  int game_timeLeft_;
  int game_timerAccum_;
  std::vector<unsigned int> round_currentOrder_;
  unsigned int round_currentStep_;

  sf::Color flashColor_;
  int flashAlpha_;
  sf::RectangleShape flashRect_;

  int pauseTextBlinkTime_;
  bool pauseTextBlinkDraw_;

  // tiles
  std::vector< std::vector<Tile> > tiles_;

  // pause shader
  bool pauseShader_created_;
  std::unique_ptr<sf::Shader> pauseShader_shader_;
  sf::RenderTexture pauseShader_renderTexture_;
  sf::Texture pauseShader_texture_;
  sf::Sprite pauseShader_sprite_;

  // selection rect
  sf::RectangleShape menuSelectionRect_;

  // title
  std::vector<sf::RectangleShape> titleRects_;
  std::vector<Text> titleLabelsTexts_;
  std::vector<Text> titleValuesTexts_;

  // help
  sf::RectangleShape helpRect_;
  std::vector<Text> helpTexts_;

  // about
  sf::RectangleShape aboutRect_;
  std::vector<Text> aboutTexts_;

  // settings
  std::vector<sf::RectangleShape> settingsRects_;
  std::vector<Text> settingsLabelsTexts_;
  std::vector<Text> settingsValuesTexts_;

  // game info
  sf::RectangleShape currentGameInfoRect_;
  std::vector<Text> currentGameInfoTexts_;

  // pause
  std::vector<sf::RectangleShape> pauseRects_;
  std::vector<Text> pauseTexts_;

  // finished
  std::vector<sf::RectangleShape> finishedRects_;
  std::vector<Text> finishedTexts_;

};

#endif