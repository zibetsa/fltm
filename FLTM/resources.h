#ifndef RESOURCES_H_INCLUDED
#define RESOURCES_H_INCLUDED

#include <memory>
#include <map>

#include <SFML/Graphics.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

class Resources {
 public:
  enum class Texture {
    DUMMY,

    FONT,
    TILES
  };

  enum class Sound {
    MENU_SELECT,
    MENU_CONFIRM,
    PAUSE,
    ANSWER_CORRECT,
    ANSWER_INCORRECT,
    FINISHED,

    NUMBER_OF_SOUNDS,
    DUMMY
  };

  Resources();
  virtual ~Resources();

  const sf::Uint8 *getAppIconData();
  sf::Texture &getTexture(const Texture texture);
  sf::SoundBuffer &getSoundBuffer(const Sound sound);
  std::unique_ptr<sf::Shader> getShader_gaussianBlur();

 private:
  void loadAllTextures();
  void loadAllSounds();

  sf::Texture texture_dummy_;
  sf::Texture texture_font_;
  sf::Texture texture_tiles_;

  sf::SoundBuffer sound_dummy_;
  std::map<Sound, sf::SoundBuffer> sounds_;

};

#endif