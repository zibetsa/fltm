#include <cstdlib>
#include <math.h>

#include "utilities.h"

namespace utils {

const float getPi() {
  return 3.14159265f;
}

const float getTileSize() {
  return 20.0f;
}

const int getRandomInt(int min, int max) {
  return std::rand() % (max - min + 1) + min;
}

const float getRandomFloat(float min, float max) {
  return ((static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * (max - min)) + min;
}

const float degreesToRadians(const float angleDegrees) {
  return angleDegrees * (getPi() / 180);
}

const float radiansToDegrees(const float angleRadians) {
  return (angleRadians / getPi()) * 180;
}

sf::RectangleShape createRect(const sf::Vector2f pos, const sf::Vector2f dim, const sf::Color color, const float outline, const sf::Color outlineColor) {
  sf::RectangleShape rect(dim);
	rect.setPosition(pos);
  rect.setFillColor(color);
  rect.setOutlineThickness(outline);
	rect.setOutlineColor(outlineColor);
  return rect;
}

sf::RectangleShape createTexturedRect(const sf::Vector2f pos, const sf::Vector2f dim, const sf::Color color, const sf::Texture &texture, const sf::IntRect textureRect) {
  sf::RectangleShape rect(dim);
  rect.setPosition(pos);
  rect.setTexture(&texture);
  rect.setTextureRect(textureRect);
  rect.setFillColor(color);
  return rect;
}

void spring(float &value, float &velocity, const float targetValue, const float dampingRatio, const float angularFrequency, const float timeStep) {
  const float f = 1.0f + 2.0f * timeStep * dampingRatio * angularFrequency;
  const float oo = angularFrequency * angularFrequency;
  const float hoo = timeStep * oo;
  const float hhoo = timeStep * hoo;
  const float detInv = 1.0f / (f + hhoo);
  const float detX = f * value + timeStep * velocity + hhoo * targetValue;
  const float detV = velocity + hoo * (targetValue - value);
  value = detX * detInv;
  velocity = detV * detInv;
}

const bool getChance(const int number) {
  if (number < 0) { return false; }
  if (number >= 100) { return true; }
  return ((rand() % 101) <= number);
}

}