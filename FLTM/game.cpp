#include <algorithm>
#include <numeric>

#include <SFML/Graphics.hpp>

#include "game.h"
#include "gameinfo.h"
#include "utilities.h"

Game::Game() {
}

Game::Game(sf::RenderWindow *window, Resources *resources, Settings *settings, SoundSystem *soundSystem) : 
  window_(window),
  resources_(resources),
  settings_(settings),
  soundSystem_(soundSystem),
  state_(State::TITLE),
  mousePos_(sf::Vector2f(0.0f, 0.0f)),

  menuSelectionRectDraw_(false),
  menuSelectionIndex_(0),
  menu_settings_showApplyButton_(false),

  game_currentRound_(0),
  game_numAnswersCorrect_(0),
  game_numAnswersWrong_(0),
  game_timeLeft_(0),

  round_currentStep_(0),

  flashColor_(sf::Color()),
  flashAlpha_(0),

  pauseTextBlinkTime_(0),
  pauseTextBlinkDraw_(true),

  pauseShader_created_(false)
{
  {
    camera_.setRect(settings_->getWindowRect());
    camera_.updateViewPort(settings_->getWindowSize());
    camera_.reset(camera_.getRect());
  }
  {
    shakeCamera_.resize(static_cast<int>(Camera::ShakeType::NUMBER_OF_SHAKE_TYPES), false);
  }
  {
    for (std::size_t i = 0; i < 4; ++i) {
      round_currentOrder_.push_back(i);
    }

  }
  {
    create();
  }
  {
    titleValuesTexts_[0].setString(settings_->game_getDifficultyAsString());
    titleValuesTexts_[1].setString(std::to_string(settings_->game_getRoundsAsValue()));
    titleValuesTexts_[2].setString(settings_->game_getTimerEnabled() ? "on" : "off");
    menu_title_updateTextsPos();
  }
  {
    settingsValuesTexts_[0].setString(settings_->getFpsLimited() ? "on" : "off");
    settingsValuesTexts_[1].setString(settings_->getFpsLimited() ? std::to_string(settings_->getFps()) : "-");
    settingsValuesTexts_[2].setString(settings_->getFpsLimited() ? "-" : (settings_->getVsync() ? "on" : "off"));
    settingsValuesTexts_[3].setString(settings_->getSfxEnabled() ? "on" : "off");
    settingsValuesTexts_[4].setString(std::to_string(settings_->getSfxVolume()));
    menu_settings_updateTextsPos();
  }
  {
    initTitle();
  }
}

Game::~Game() {
}

void Game::processEvents() {
  sf::Event evt;

  while (window_->pollEvent(evt)) {

    switch (evt.type) {

      case sf::Event::Closed:
        {
          exitProgram();
        }
        break;

      case sf::Event::GainedFocus:
        {
          settings_->setWindowIsFocused(true);
        }
        break;

      case sf::Event::LostFocus:
        {
          settings_->setWindowIsFocused(false);
          switch (state_) {
            case State::RUNNING: { pauseGame(); } break;
            case State::PAUSED:  { pauseTextBlinkTime_ = 0; pauseTextBlinkDraw_ = true; } break;
            default: {} break;
          }

        }
        break;

      case sf::Event::MouseButtonPressed:
        {
          switch (evt.mouseButton.button) {
            case sf::Mouse::Button::Left:  { menu_select(); } break;
            default: {} break;
          }
        }
        break;

      case sf::Event::KeyPressed:
        {
          switch (state_) {

            case State::TITLE:
              {
                switch (evt.key.code) {
                  case sf::Keyboard::Return: { menu_select(); } break;
                  default: {} break;
                }
              }
              break;

            case State::HELP:
            case State::ABOUT:
              {
                switch (evt.key.code) {
                  case sf::Keyboard::Escape:
                  case sf::Keyboard::Return:
                    {
                      menu_select();
                    } break;
                  default: {} break;
                }
              }
              break;

            case State::RUNNING:
              {
                switch (evt.key.code) {
                  case sf::Keyboard::Escape: { pauseGame(); } break;
                  case sf::Keyboard::Up:
                  case sf::Keyboard::W:
                  case sf::Keyboard::Numpad8:
                    {
                      pressedButton(SelectedGroup::UP);
                    } break;
                  case sf::Keyboard::Right:
                  case sf::Keyboard::D:
                  case sf::Keyboard::Numpad6:
                    {
                      pressedButton(SelectedGroup::RIGHT);
                    } break;
                  case sf::Keyboard::Down:
                  case sf::Keyboard::S:
                  case sf::Keyboard::Numpad5:
                    {
                      pressedButton(SelectedGroup::DOWN);
                    } break;
                  case sf::Keyboard::Left:
                  case sf::Keyboard::A:
                  case sf::Keyboard::Numpad4:
                    {
                      pressedButton(SelectedGroup::LEFT);
                    } break;
                  default: {} break;
                }
              }
              break;

            case State::PAUSED:
              {
                switch (evt.key.code) {
                  case sf::Keyboard::Escape: { resumeGame(); } break;
                  case sf::Keyboard::Return: { menu_select(); } break;
                  default: {} break;
                }
              }
              break;

            case State::FINISHED:
              {
                switch (evt.key.code) {
                  case sf::Keyboard::Return: { menu_select(); } break;
                  default: {} break;
                }
              }
              break;

            default:
              {
              }
              break;

          }
        }
        break;

      default:
        {
        }
        break;

    }

  }
}

void Game::update(sf::Time dt) {
  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      tiles_[i][j].update(dt);
    }
  }

  if (!settings_->getWindowIsFocused()) {
    return;
  }

  mousePos_ = window_->mapPixelToCoords(sf::Mouse::getPosition(*window_));

  for (std::size_t i = 0; i < shakeCamera_.size(); ++i) {
    const Camera::ShakeType shakeType = static_cast<Camera::ShakeType>(i);
    if (shakeCamera_[static_cast<int>(shakeType)]) {
      camera_.shake(shakeType);
      shakeCamera_[static_cast<int>(shakeType)] = false;
    }
  }

  if (state_ != State::PAUSED) {
    camera_.update(dt);
  }

  if (flashAlpha_ > 0) {
    flashAlpha_ -= std::max(dt.asMilliseconds() / 2, 8);
    if (flashAlpha_ <= 0) {
      flashAlpha_ = 0;
    }
    flashColor_.a = flashAlpha_;
    flashRect_.setFillColor(flashColor_);
  }

  switch (state_) {

    case State::TITLE:
      {
        menuSelectionRectDraw_ = false;
        for (std::size_t i = 0; i < titleRects_.size(); ++i) {
          if (titleRects_[i].getGlobalBounds().contains(mousePos_)) {
            menuSelectionRectDraw_ = true;
            menuSelectionRect_.setPosition(titleRects_[i].getPosition());
            menuSelectionRect_.setSize(titleRects_[i].getSize());
            break;
          }
        }
        if (!menuSelectionRectDraw_) {
          const std::size_t maxIndex = menu_settings_showApplyButton_ ? settingsRects_.size() : settingsRects_.size() - 1;
          for (std::size_t i = 0; i < maxIndex; ++i) {
            if (settingsRects_[i].getGlobalBounds().contains(mousePos_)) {
              menuSelectionRectDraw_ = true;
              menuSelectionRect_.setPosition(settingsRects_[i].getPosition());
              menuSelectionRect_.setSize(settingsRects_[i].getSize());
              break;
            }
          }
        }
      }
      break;

    case State::HELP:
      {
      }
      break;

    case State::ABOUT:
      {
      }
      break;

    case State::RUNNING:
      {
        if (settings_->game_getTimerEnabled()) {
          game_timerAccum_ += dt.asMilliseconds();
          if (game_timerAccum_ >= 1000) {
            game_timerAccum_ -= 1000;
            --game_timeLeft_;
            if (game_timeLeft_ <= 0) {
              genNewRoundTime();
              game_timerAccum_ = 0;
              pressedButton(Game::SelectedGroup::UP, true);
            } else {
              updateGameInfo(false, false, false, true);
            }
          }
        }
      }
      break;

    case State::PAUSED:
      {
        {
          pauseTextBlinkTime_ += dt.asMilliseconds();
          if (pauseTextBlinkTime_ >= 400) {
            pauseTextBlinkTime_ = 0;
            pauseTextBlinkDraw_ = !pauseTextBlinkDraw_;
          }
        }
        menuSelectionRectDraw_ = false;
        for (std::size_t i = 0; i < pauseRects_.size(); ++i) {
          if (pauseRects_[i].getGlobalBounds().contains(mousePos_)) {
            menuSelectionRectDraw_ = true;
            menuSelectionRect_.setPosition(pauseRects_[i].getPosition());
            menuSelectionRect_.setSize(pauseRects_[i].getSize());
            break;
          }
        }
        if (!menuSelectionRectDraw_) {
          const std::size_t maxIndex = menu_settings_showApplyButton_ ? settingsRects_.size() : settingsRects_.size() - 1;
          for (std::size_t i = 0; i < maxIndex; ++i) {
            if (settingsRects_[i].getGlobalBounds().contains(mousePos_)) {
              menuSelectionRectDraw_ = true;
              menuSelectionRect_.setPosition(settingsRects_[i].getPosition());
              menuSelectionRect_.setSize(settingsRects_[i].getSize());
              break;
            }
          }
        }
      }
      break;

    case State::FINISHED:
      {
        menuSelectionRectDraw_ = false;
        for (std::size_t i = 0; i < finishedRects_.size(); ++i) {
          if (finishedRects_[i].getGlobalBounds().contains(mousePos_)) {
            menuSelectionRectDraw_ = true;
            menuSelectionRect_.setPosition(finishedRects_[i].getPosition());
            menuSelectionRect_.setSize(finishedRects_[i].getSize());
            break;
          }
        }
      }
      break;

    default:
      {
      }
      break;

  }
}

void Game::render() {
  window_->clear();
  window_->setView(camera_.getView());
  render_game();
  render_menu();
  window_->display();
}

void Game::create() {
  const sf::FloatRect winRect = settings_->getWindowRect();

  flashRect_ = utils::createRect(sf::Vector2f(-20.0f, -20.0f), sf::Vector2f(winRect.width + 40.0f, winRect.height + 40.0f), sf::Color(0, 0, 0), 0.0f, sf::Color(0, 0, 0));

  // tiles
  {
    tiles_.clear();
    tiles_.resize(4);
    
    const std::size_t linesPerColor = 10;
    const std::size_t maxCount = 29;
    const float border = 15.0f;
    const float offset = 5.0f;

    // top
    {
      sf::Vector2f pos(0.0f, winRect.top + border + 10.0f);
      std::size_t count = maxCount;
      for (std::size_t i = 0; i < linesPerColor; ++i) {
        pos.x = winRect.left + border + 10.0f + (utils::getTileSize() + offset) + (utils::getTileSize() + offset) * i;
        for (std::size_t j = 0; j < count; ++j) {
          tiles_[0].push_back(Tile(pos, resources_->getTexture(Resources::Texture::TILES), settings_->getWindowRect()));
          tiles_[0].back().setType(Tile::Type::YELLOW);
          pos.x += (utils::getTileSize() + offset);
        }
        pos.y += (utils::getTileSize() + offset);
        count -= 2;
      }
    }

    // right
    {
      sf::Vector2f pos(winRect.left + winRect.width - border + 10.0f - utils::getTileSize(), 0.0f);
      std::size_t count = maxCount;
      for (std::size_t i = 0; i < linesPerColor; ++i) {
        pos.y = winRect.top + border + 10.0f + (utils::getTileSize() + offset) + i * (utils::getTileSize() + offset);
        for (std::size_t j = 0; j < count; ++j) {
          tiles_[1].push_back(Tile(pos, resources_->getTexture(Resources::Texture::TILES), settings_->getWindowRect()));
          tiles_[1].back().setType(Tile::Type::RED);
          pos.y += (utils::getTileSize() + offset);
        }
        pos.x -= (utils::getTileSize() + offset);
        count -= 2;
      }
    }

    // bottom
    {
      sf::Vector2f pos(0.0f, winRect.top + winRect.height - border + 10.0f - utils::getTileSize());
      std::size_t count = maxCount;
      for (std::size_t i = 0; i < linesPerColor; ++i) {
        pos.x = winRect.left + border + 10.0f + (utils::getTileSize() + offset) + (utils::getTileSize() + offset) * i;
        for (std::size_t j = 0; j < count; ++j) {
          tiles_[2].push_back(Tile(pos, resources_->getTexture(Resources::Texture::TILES), settings_->getWindowRect()));
          tiles_[2].back().setType(Tile::Type::GREEN);
          pos.x += (utils::getTileSize() + offset);
        }
        pos.y -= (utils::getTileSize() + offset);
        count -= 2;
      }
    }

    // left
    {
      sf::Vector2f pos(winRect.left + border + 10.0f, 0.0f);
      std::size_t count = maxCount;
      for (std::size_t i = 0; i < linesPerColor; ++i) {
        pos.y = winRect.top + border + 10.0f + (utils::getTileSize() + offset) + i * (utils::getTileSize() + offset);
        for (std::size_t j = 0; j < count; ++j) {
          tiles_[3].push_back(Tile(pos, resources_->getTexture(Resources::Texture::TILES), settings_->getWindowRect()));
          tiles_[3].back().setType(Tile::Type::BLUE);
          pos.y += (utils::getTileSize() + offset);
        }
        pos.x += (utils::getTileSize() + offset);
        count -= 2;
      }
    }
  }

  // pause shader
  {
    if (sf::Shader::isAvailable()) {
      pauseShader_created_ = pauseShader_renderTexture_.create(static_cast<unsigned int>(winRect.width), static_cast<unsigned int>(winRect.height));
      if (pauseShader_created_) {
        pauseShader_shader_ = resources_->getShader_gaussianBlur();
        pauseShader_shader_->setUniform("sourceTexture", sf::Shader::CurrentTexture);
        pauseShader_shader_->setUniform("offsetFactor", sf::Vector2f(1.0f / winRect.width, 1.0f / winRect.height));
        pauseShader_sprite_.setPosition(sf::Vector2f(0.0f, 0.0f));
      }
    }
  }

  // selection rect
  {
    menuSelectionRect_ = utils::createRect(sf::Vector2f(0.0f, 0.0f), sf::Vector2f(0.0f, 0.0f), sf::Color(255, 100, 0), 0.0f, sf::Color(0, 0, 0));
  }

  // title
  {
    {
      titleLabelsTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), gameinfo::NAME, Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 2.0f, sf::Color(255, 255, 255)));
      titleLabelsTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - titleLabelsTexts_.back().getWidth() / 2 + 5.0f)), 
        static_cast<float>(static_cast<int>(winRect.top + 50.0f))
      ));
    }
    {
      titleLabelsTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "::: " + gameinfo::NAME_ADV + " :::", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.4f, sf::Color(80, 160, 100)));
      titleLabelsTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - titleLabelsTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(winRect.top + titleLabelsTexts_[0].getPos().y + titleLabelsTexts_[0].getHeight() + 20.0f))
      ));
    }
    for (std::size_t i = 0; i < 3; ++i) {
      {
        titleLabelsTexts_.push_back(Text(
          sf::Vector2f(static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - 166.0f)), static_cast<float>(static_cast<int>(winRect.top + winRect.height / 2 - 55.0f + 45.0f * i))), 
          "<", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.4f, sf::Color(255, 255, 255)
        ));
        titleLabelsTexts_.push_back(Text(
          sf::Vector2f(static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - 19.0f)), static_cast<float>(static_cast<int>(winRect.top + winRect.height / 2 - 55.0f + 45.0f * i))), 
          ">", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.4f, sf::Color(255, 255, 255)
        ));
      }
      {
        std::string newString = "";
        switch (i) {
          case 0:  { newString = "Difficulty:"; } break;
          case 1:  { newString = "Rounds:"; } break;
          case 2:  { newString = "Timer:"; } break;
          default: {} break;
        }
        titleLabelsTexts_.push_back(Text(
          sf::Vector2f(static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - 335.0f)), static_cast<float>(static_cast<int>(winRect.top + winRect.height / 2 - 53.0f + 45.0f * i))), 
          newString, Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.4f, sf::Color(255, 255, 255)
        ));
      }
      {
        titleRects_.push_back(utils::createRect(
          sf::Vector2f(winRect.left + winRect.width / 2 - 172.0f, winRect.top + winRect.height / 2 - 65.0f + 45.0f * i), 
          sf::Vector2f(170.0f, 40.0f), sf::Color(50, 50, 50, 220), 0.0f, sf::Color()
        ));
      }
      {
        titleValuesTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), " ", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.4f, sf::Color(255, 255, 255)));
      }
    }
    {
      titleRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left + winRect.width / 2 + 3.0f, winRect.top + winRect.height / 2 - 65.0f), 
        sf::Vector2f(330.0f, 130.0f), sf::Color(50, 50, 50, 220), 0.0f, sf::Color()
      ));
      titleLabelsTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Play >>>", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.5f, sf::Color(255, 255, 255)));
      titleLabelsTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().left + titleRects_.back().getGlobalBounds().width / 2 - titleLabelsTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().top + titleRects_.back().getGlobalBounds().height / 2 - titleLabelsTexts_.back().getHeight() / 2))
      ));
    }
    {
      titleRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left + winRect.width - 150.0f - 10.0f, winRect.top + winRect.height - 135.0f - 20.0f), 
        sf::Vector2f(150.0f, 45.0f), sf::Color(50, 50, 50, 220), 0.0f, sf::Color()
      ));
      titleLabelsTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Help", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      titleLabelsTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().left + titleRects_.back().getGlobalBounds().width / 2 - titleLabelsTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().top + titleRects_.back().getGlobalBounds().height / 2 - titleLabelsTexts_.back().getHeight() / 2))
      ));
    }
    {
      titleRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left + winRect.width - 150.0f - 10.0f, winRect.top + winRect.height - 90.0f - 15.0f), 
        sf::Vector2f(150.0f, 45.0f), sf::Color(50, 50, 50, 200), 0.0f, sf::Color()
      ));
      titleLabelsTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "About", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      titleLabelsTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().left + titleRects_.back().getGlobalBounds().width / 2 - titleLabelsTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().top + titleRects_.back().getGlobalBounds().height / 2 - titleLabelsTexts_.back().getHeight() / 2))
      ));
    }
    {
      titleRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left + winRect.width - 150.0f - 10.0f, winRect.top + winRect.height - 45.0f - 10.0f), 
        sf::Vector2f(150.0f, 45.0f), sf::Color(150, 0, 0, 200), 0.0f, sf::Color()
      ));
      titleLabelsTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Exit", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      titleLabelsTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().left + titleRects_.back().getGlobalBounds().width / 2 - titleLabelsTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(titleRects_.back().getGlobalBounds().top + titleRects_.back().getGlobalBounds().height / 2 - titleLabelsTexts_.back().getHeight() / 2))
      ));
    }
  }

  // help
  {
    helpRect_ = utils::createRect(
      sf::Vector2f(winRect.left - 10.0f, winRect.top + winRect.height / 2 - 105.0f), 
      sf::Vector2f(winRect.width + 20.0f, 210.0f), sf::Color(80, 160, 100), 6.0f, sf::Color(50, 100, 65)
    );
    {
      std::vector<std::string> lineStrings;
      lineStrings.push_back("HOW TO PLAY");
      lineStrings.push_back(" ");
      lineStrings.push_back("Select the colored groups");
      lineStrings.push_back("of tiles in order, from the ");
      lineStrings.push_back("group with the fewest tiles");
      lineStrings.push_back("to the one with the most.");
      lineStrings.push_back("CONTROLS");
      lineStrings.push_back(" ");
      lineStrings.push_back("Select top:");
      lineStrings.push_back("Select right:");
      lineStrings.push_back("Select bottom:");
      lineStrings.push_back("Select left:");
      lineStrings.push_back("ArrUp, W, Num8");
      lineStrings.push_back("ArrRt, D, Num6");
      lineStrings.push_back("ArrDn, S, Num5");
      lineStrings.push_back("ArrLt, A, Num4");
      {
        sf::Vector2f pos(helpRect_.getGlobalBounds().left + 35.0f, helpRect_.getGlobalBounds().top + 35.0f);
        for (std::size_t i = 0; i < lineStrings.size(); ++i) {
          const float size = ((i == 0) || (i == 6)) ? 0.6f : 0.3f;
          const sf::Color color = ((i == 0) || (i == 6)) ? sf::Color(0, 0, 0) : ((i > 11) ? sf::Color(45, 65, 85) : sf::Color(255, 255, 255));
          helpTexts_.push_back(Text(
            sf::Vector2f(static_cast<float>(static_cast<int>(pos.x)), static_cast<float>(static_cast<int>(pos.y))), 
            lineStrings[i], Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), size, color
          ));
          pos.y += 26.0f;
          if (i == 5) {
            pos.x += 380.0f;
            pos.y = helpRect_.getGlobalBounds().top + 35.0f;
          }
          if (i == 11) {
            pos.x += 190.0f;
            pos.y = helpRect_.getGlobalBounds().top + 35.0f + 26.0f * 2;
          }
        }
      }
    }
  }

  // about
  {
    aboutRect_ = utils::createRect(
      sf::Vector2f(winRect.left - 10.0f, winRect.top + winRect.height / 2 - 105.0f), 
      sf::Vector2f(winRect.width + 20.0f, 210.0f), sf::Color(80, 160, 100), 6.0f, sf::Color(50, 100, 65)
    );
    {
      std::vector<std::string> lineStrings;
      lineStrings.push_back(gameinfo::NAME);
      lineStrings.push_back(" ");
      lineStrings.push_back("v" + gameinfo::VERSION + " (" + gameinfo::DATE + ")");
      lineStrings.push_back(" ");
      lineStrings.push_back("A game by " + gameinfo::AUTHOR);
      lineStrings.push_back(gameinfo::WEBSITE);
      {
        for (std::size_t i = 0; i < lineStrings.size(); ++i) {
          aboutTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), lineStrings[i], Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), (i == 0) ? 0.7f : 0.4f, sf::Color(255, 255, 255)));
          aboutTexts_.back().setPos(sf::Vector2f(
            static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - aboutTexts_.back().getWidth() / 2)), 
            static_cast<float>(static_cast<int>(aboutRect_.getGlobalBounds().top + 35.0f + 28.0f * i))
          ));
        }
      }
    }
  }

  // settings
  {
    for (std::size_t i = 0; i < 6; ++i) {
      {
        settingsLabelsTexts_.push_back(Text(
          sf::Vector2f(static_cast<float>(static_cast<int>(winRect.left + 145.0f)), static_cast<float>(static_cast<int>(winRect.top + winRect.height - 173.0f + 35.0f * i))), 
          "<", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)
        ));
        settingsLabelsTexts_.push_back(Text(
          sf::Vector2f(static_cast<float>(static_cast<int>(winRect.left + 262.0f)), static_cast<float>(static_cast<int>(winRect.top + winRect.height - 173.0f + 35.0f * i))), 
          ">", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)
        ));
      }
      {
        std::string newString = "";
        switch (i) {
          case 0:  { newString = "FPS limit:"; } break;
          case 1:  { newString = "Limit:"; } break;
          case 2:  { newString = "VSync:"; } break;
          case 3:  { newString = "SFX:"; } break;
          case 4:  { newString = "SFX vol:"; } break;
          default: {} break;
        }
        if (i < 5) {
          settingsLabelsTexts_.push_back(Text(
            sf::Vector2f(static_cast<float>(static_cast<int>(winRect.left + 15.0f)), static_cast<float>(static_cast<int>(winRect.top + winRect.height - 173.0f + 35.0f * i))), 
            newString, Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)
          ));
        }
      }
      {
        sf::Vector2f pos(winRect.left + 140.0f, winRect.top + winRect.height - 180.0f + 35.0f * i);
        if (i == 5) {
          settingsRects_.push_back(utils::createRect(pos + sf::Vector2f(140.0f, -(35.0f * i)), sf::Vector2f(135.0f, 100.0f), sf::Color(0, 255, 0, 220), 0.0f, sf::Color()));
        } else {
          settingsRects_.push_back(utils::createRect(pos, sf::Vector2f(135.0f, 30.0f), sf::Color(50, 50, 50, 220), 0.0f, sf::Color()));
        }
      }
      {
        settingsValuesTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), (i == 5) ? "Apply" : " ", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      }
    }
  }

  // game info
  {
    currentGameInfoRect_ = utils::createRect(
      sf::Vector2f(winRect.left + winRect.width / 2 - 125.0f, winRect.top + winRect.height / 2 - 125.0f), 
      sf::Vector2f(250.0f, 250.0f), sf::Color(50, 50, 50, 220), -4.0f, sf::Color(50, 50, 50, 160)
    );
    {
      const sf::FloatRect rBounds = currentGameInfoRect_.getGlobalBounds();
      currentGameInfoTexts_.push_back(Text(sf::Vector2f(rBounds.left + 10.0f, rBounds.top + 10.0f), "", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      currentGameInfoTexts_.push_back(Text(sf::Vector2f(rBounds.left + 10.0f, rBounds.top + 40.0f), "", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      currentGameInfoTexts_.push_back(Text(sf::Vector2f(rBounds.left + 10.0f, rBounds.top + 104.0f), "Correct answers:", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      currentGameInfoTexts_.push_back(Text(sf::Vector2f(rBounds.left + 10.0f, rBounds.top + 134.0f), "", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      currentGameInfoTexts_.push_back(Text(sf::Vector2f(rBounds.left + 10.0f, rBounds.top + 222.0f), "", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
    }
  }

  // pause
  {
    {
      pauseTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "- Game paused -", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.5f, sf::Color(255, 255, 255)));
      pauseTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - pauseTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(winRect.top + winRect.height / 2 - 110.0f - pauseTexts_.back().getHeight()))
      ));
    }
    {
      pauseRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left, winRect.top + winRect.height / 2 - 100.0f), sf::Vector2f(winRect.width, 60.0f), 
        sf::Color(50, 50, 50, 220), 0.0f, sf::Color()
      ));
      pauseTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Resume", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.5f, sf::Color(255, 255, 255)));
      pauseTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(pauseRects_.back().getGlobalBounds().left + pauseRects_.back().getGlobalBounds().width / 2 - pauseTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(pauseRects_.back().getGlobalBounds().top + pauseRects_.back().getGlobalBounds().height / 2 - pauseTexts_.back().getHeight() / 2))
      ));
    }
    {
      pauseRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left, winRect.top + winRect.height / 2 - 30.0f), sf::Vector2f(winRect.width, 60.0f), 
        sf::Color(50, 50, 50, 220), 0.0f, sf::Color()
      ));
      pauseTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Restart", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.5f, sf::Color(255, 255, 255)));
      pauseTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(pauseRects_.back().getGlobalBounds().left + pauseRects_.back().getGlobalBounds().width / 2 - pauseTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(pauseRects_.back().getGlobalBounds().top + pauseRects_.back().getGlobalBounds().height / 2 - pauseTexts_.back().getHeight() / 2))
      ));
    }
    {
      pauseRects_.push_back(utils::createRect(
        sf::Vector2f(winRect.left, winRect.top + winRect.height / 2 + 40.0f), sf::Vector2f(winRect.width, 60.0f), 
        sf::Color(50, 50, 50, 220), 0.0f, sf::Color()
      ));
      pauseTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Quit current run", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.5f, sf::Color(255, 255, 255)));
      pauseTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(pauseRects_.back().getGlobalBounds().left + pauseRects_.back().getGlobalBounds().width / 2 - pauseTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(pauseRects_.back().getGlobalBounds().top + pauseRects_.back().getGlobalBounds().height / 2 - pauseTexts_.back().getHeight() / 2))
      ));
    }
  }

  // finished
  {
    const sf::FloatRect rBounds = currentGameInfoRect_.getGlobalBounds();
    {
      finishedRects_.push_back(utils::createRect(
        sf::Vector2f(rBounds.left + 10.0f, rBounds.top + rBounds.height - 40.0f), sf::Vector2f(110.0f, 30.0f), 
        sf::Color(100, 100, 100, 220), 0.0f, sf::Color()
      ));
      finishedTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Restart", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      finishedTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(finishedRects_.back().getGlobalBounds().left + finishedRects_.back().getGlobalBounds().width / 2 - finishedTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(finishedRects_.back().getGlobalBounds().top + finishedRects_.back().getGlobalBounds().height / 2 - finishedTexts_.back().getHeight() / 2))
      ));
    }
    {
      finishedRects_.push_back(utils::createRect(
        sf::Vector2f(rBounds.left + rBounds.width - 120.0f, rBounds.top + rBounds.height - 40.0f), sf::Vector2f(110.0f, 30.0f), 
        sf::Color(100, 100, 100, 220), 0.0f, sf::Color()
      ));
      finishedTexts_.push_back(Text(sf::Vector2f(0.0f, 0.0f), "Quit", Text::Font::DEFAULT, resources_->getTexture(Resources::Texture::FONT), 0.3f, sf::Color(255, 255, 255)));
      finishedTexts_.back().setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(finishedRects_.back().getGlobalBounds().left + finishedRects_.back().getGlobalBounds().width / 2 - finishedTexts_.back().getWidth() / 2)), 
        static_cast<float>(static_cast<int>(finishedRects_.back().getGlobalBounds().top + finishedRects_.back().getGlobalBounds().height / 2 - finishedTexts_.back().getHeight() / 2))
      ));
    }
  }
}

void Game::render_game() {
  if (flashAlpha_> 0) {
    window_->draw(flashRect_);
  }

  switch (state_) {

    case State::TITLE:
    case State::HELP:
    case State::ABOUT:
      {
        render_game_tiles(*window_);
      }
      break;

    case State::RUNNING:
    case State::FINISHED:
      {
        render_game_tiles(*window_);
        render_game_info(*window_);
      }
      break;

    case State::PAUSED:
      {
        if (pauseShader_created_) {
          pauseShader_renderTexture_.clear();
          render_game_tiles(pauseShader_renderTexture_);
          pauseShader_renderTexture_.display();
          pauseShader_sprite_.setTexture(pauseShader_renderTexture_.getTexture());
          window_->draw(pauseShader_sprite_, pauseShader_shader_.get());
        } else {
          render_game_tiles(*window_);
        }
      }
      break;

    default:
      {
      }
      break;

  }
}

void Game::render_game_tiles(sf::RenderTarget &renderTarget) {
  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      renderTarget.draw(tiles_[i][j]);
    }
  }
}

void Game::render_game_info(sf::RenderTarget &renderTarget) {
  renderTarget.draw(currentGameInfoRect_);
  for (std::size_t i = 0; i < ((state_ == State::FINISHED) ? (currentGameInfoTexts_.size() - 1) : currentGameInfoTexts_.size()); ++i) {
    renderTarget.draw(currentGameInfoTexts_[i]);
  }
}

void Game::render_menu() {
  switch (state_) {

    case State::TITLE:
    case State::HELP:
    case State::ABOUT:
      {
        for (std::size_t i = 0; i < titleRects_.size(); ++i) {
          window_->draw(titleRects_[i]);
        }
        for (std::size_t i = 0; i < (menu_settings_showApplyButton_ ? settingsRects_.size() : settingsRects_.size() - 1); ++i) {
          window_->draw(settingsRects_[i]);
        }
        if (menuSelectionRectDraw_) {
          window_->draw(menuSelectionRect_);
        }
        for (std::size_t i = 0; i < titleLabelsTexts_.size(); ++i) {
          window_->draw(titleLabelsTexts_[i]);
        }
        for (std::size_t i = 0; i < titleValuesTexts_.size(); ++i) {
          window_->draw(titleValuesTexts_[i]);
        }
        for (std::size_t i = 0; i < (menu_settings_showApplyButton_ ? settingsLabelsTexts_.size() : settingsLabelsTexts_.size() - 1); ++i) {
          window_->draw(settingsLabelsTexts_[i]);
        }
        for (std::size_t i = 0; i < (menu_settings_showApplyButton_ ? settingsValuesTexts_.size() : settingsValuesTexts_.size() - 1); ++i) {
          window_->draw(settingsValuesTexts_[i]);
        }
        if (state_ == State::HELP) {
          window_->draw(helpRect_);
          for (std::size_t i = 0; i < helpTexts_.size(); ++i) {
            window_->draw(helpTexts_[i]);
          }
        } else if (state_ == State::ABOUT) {
          window_->draw(aboutRect_);
          for (std::size_t i = 0; i < aboutTexts_.size(); ++i) {
            window_->draw(aboutTexts_[i]);
          }
        }
      }
      break;

    case State::RUNNING:
      {
      }
      break;

    case State::PAUSED:
      {
        for (std::size_t i = 0; i < pauseRects_.size(); ++i) {
          window_->draw(pauseRects_[i]);
        }
        for (std::size_t i = 0; i < (menu_settings_showApplyButton_ ? settingsRects_.size() : settingsRects_.size() - 1); ++i) {
          window_->draw(settingsRects_[i]);
        }
        if (menuSelectionRectDraw_) {
          window_->draw(menuSelectionRect_);
        }
        for (std::size_t i = (pauseTextBlinkDraw_ ? 0 : 1); i < pauseTexts_.size(); ++i) {
          window_->draw(pauseTexts_[i]);
        }
        for (std::size_t i = 0; i < (menu_settings_showApplyButton_ ? settingsLabelsTexts_.size() : settingsLabelsTexts_.size() - 1); ++i) {
          window_->draw(settingsLabelsTexts_[i]);
        }
        for (std::size_t i = 0; i < (menu_settings_showApplyButton_ ? settingsValuesTexts_.size() : settingsValuesTexts_.size() - 1); ++i) {
          window_->draw(settingsValuesTexts_[i]);
        }
      }
      break;

    case State::FINISHED:
      {
        for (std::size_t i = 0; i < finishedRects_.size(); ++i) {
          window_->draw(finishedRects_[i]);
        }
        if (menuSelectionRectDraw_) {
          window_->draw(menuSelectionRect_);
        }
        for (std::size_t i = 0; i < finishedTexts_.size(); ++i) {
          window_->draw(finishedTexts_[i]);
        }
      }
      break;

    default:
      {
      }
      break;

  }
}

void Game::pauseGame() {
  if (state_ != State::RUNNING) {
    return;
  }

  state_ = State::PAUSED;

  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      tiles_[i][j].resetPos(true);
    }
  }

  menuSelectionRectDraw_ = false;
  pauseTextBlinkTime_ = 0;
  pauseTextBlinkDraw_ = true;
  
  soundSystem_->playSound(Resources::Sound::PAUSE);
  soundSystem_->pauseSfx();
}

void Game::resumeGame() {
  if (state_ != State::PAUSED) {
    return;
  }

  state_ = State::RUNNING;

  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      tiles_[i][j].resetPos(false);
    }
  }

  soundSystem_->playSound(Resources::Sound::PAUSE);
  soundSystem_->resumeSfx();
}

void Game::restartGame() {
  initGame();
}

void Game::quitGame() {
  switch (state_) {

    case State::PAUSED:
    case State::FINISHED:
      {
        initTitle();
        soundSystem_->playSound(Resources::Sound::MENU_CONFIRM);
      }
      break;

    default:
      {
      }
      break;

  }
}

void Game::initTitle() {
  state_ = State::TITLE;

  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      if (utils::getChance(25)) {
        tiles_[i][j].setType(Tile::Type::NEUTRAL);
      } else {
        tiles_[i][j].setType(Tile::Type::INVIS);
      }
      tiles_[i][j].resetPos(false);
      tiles_[i][j].flipOver();
    }
  }
}

void Game::initGame() {
  state_ = State::RUNNING;
  game_currentRound_ = 1;
  game_numAnswersCorrect_ = 0;
  game_numAnswersWrong_ = 0;
  genNewRoundTime();
  game_timerAccum_ = 0;
  updateGameInfo(true, true, true, true);
  generateRound();
  soundSystem_->playSound(Resources::Sound::MENU_CONFIRM);
}

void Game::updateGameInfo(const bool updDifficulty, const bool updRounds, const bool updAnswers, const bool updTimer) {
  if (!updDifficulty && !updRounds && !updAnswers && !updTimer) {
    return;
  }

  if (updDifficulty) {
    currentGameInfoTexts_[0].setString(":: " + settings_->game_getDifficultyAsString() + " ::");
    sf::Color textColor(255, 255, 255);
    switch (settings_->game_getDifficulty()) {
      case Settings::GameDifficulty::EASY:            { textColor = sf::Color(80, 170, 80); } break;
      case Settings::GameDifficulty::MEDIUM: default: { textColor = sf::Color(170, 170, 80); } break;
      case Settings::GameDifficulty::HARD:            { textColor = sf::Color(170, 80, 80); } break;
      case Settings::GameDifficulty::INSANE:          { textColor = sf::Color(170, 80, 170); } break;
      case Settings::GameDifficulty::UNFAIR:          { textColor = sf::Color(80, 80, 170); } break;
    }
    currentGameInfoTexts_[0].setColor(textColor);
  }

  if (updRounds) {
    currentGameInfoTexts_[1].setString("Round " + std::to_string(game_currentRound_) + " / " + std::to_string(settings_->game_getRoundsAsValue()));
  }

  if (updAnswers) {
    currentGameInfoTexts_[3].setString(std::to_string(game_numAnswersCorrect_) + " / " + std::to_string((game_numAnswersCorrect_ + game_numAnswersWrong_)));
  }

  if (updTimer) {
    currentGameInfoTexts_[4].setString("Time left: " + (settings_->game_getTimerEnabled() ? std::to_string(game_timeLeft_) : "-"));
  }

  {
    const sf::FloatRect winRect = settings_->getWindowRect();
    for (std::size_t i = 0; i < currentGameInfoTexts_.size(); ++i) {
      currentGameInfoTexts_[i].setPos(sf::Vector2f(
        static_cast<float>(static_cast<int>(winRect.left + winRect.width / 2 - currentGameInfoTexts_[i].getWidth() / 2)), 
        static_cast<float>(static_cast<int>(currentGameInfoTexts_[i].getPos().y))
      ));
    }
  }
}

void Game::generateRound() {
  round_currentStep_ = 0;

  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      tiles_[i][j].setType(Tile::Type::INVIS);
    }
  }

  // step and range marker
  sf::Vector2i groupData(0, 0);
  switch (settings_->game_getDifficulty()) {
    case Settings::GameDifficulty::EASY:            { groupData = sf::Vector2i(31, 7); } break;
    case Settings::GameDifficulty::MEDIUM: default: { groupData = sf::Vector2i(23, 31); } break;
    case Settings::GameDifficulty::HARD:            { groupData = sf::Vector2i(15, 55); } break;
    case Settings::GameDifficulty::INSANE:          { groupData = sf::Vector2i(7, 79); } break;
    case Settings::GameDifficulty::UNFAIR:          { groupData = sf::Vector2i(1, 95); } break;
  }
  // 1: up, 2: down
  const int groupDirection = utils::getChance(50) ? 1 : 0;
  const int groupStartAt = (groupDirection > 0) ? utils::getRandomInt(5, groupData.y) : utils::getRandomInt(100 - groupData.y, 95);
  const int groupStep = (groupDirection > 0) ? groupData.x : -groupData.x;
  std::vector<int> group_valuesOrig;
  std::vector<int> group_valuesShuffled;

  // generate %s and shuffle
  {
    for (std::size_t i = 0; i < tiles_.size(); ++i) {
      group_valuesOrig.push_back(groupStartAt + i * groupStep);
      group_valuesShuffled.push_back(group_valuesOrig.back());
    }
    std::random_shuffle(group_valuesShuffled.begin(), group_valuesShuffled.end());
  }

  // get new order
  {
    std::vector<int> indexVector(group_valuesShuffled.size());
    std::iota(indexVector.begin(), indexVector.end(), 0);
    auto comparator = [&group_valuesShuffled](int a, int b) { return group_valuesShuffled[a] < group_valuesShuffled[b]; };
    std::sort(indexVector.begin(), indexVector.end(), comparator);
    for (std::size_t i = 0; i < indexVector.size(); ++i) {
      round_currentOrder_[i] = indexVector[i];
    }
  }

  // assign colors and keep track
  {
    //std::vector<int> tileCount;
    //tileCount.resize(tiles_.size(), 0);

    for (std::size_t i = 0; i < tiles_.size(); ++i) {
      Tile::Type curTileType;
      switch (i) {
        case 0:  { curTileType = Tile::Type::YELLOW; } break;
        case 1:  { curTileType = Tile::Type::RED; } break;
        case 2:  { curTileType = Tile::Type::GREEN; } break;
        case 3:  { curTileType = Tile::Type::BLUE; } break;
        default: { continue; } break;
      }
      for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
        if (utils::getChance(group_valuesShuffled[i])) {
          tiles_[i][j].setType(curTileType);
          //++tileCount[i];
        } else {
          tiles_[i][j].setType(Tile::Type::INVIS);
        }
      }
    }

    // todo: check and adjust tile count
    // because of tiles being randomly assigned a color, actual tile counts can differ from what was originally intended and saved in round_currentOrder_
    // if the color group difference is too small actual counts may be wrong, more likely to happen on higher difficulties
    // this is less important than other stuff and can be fixed later
    {
    }
  }

  for (std::size_t i = 0; i < tiles_.size(); ++i) {
    for (std::size_t j = 0; j < tiles_[i].size(); ++j) {
      tiles_[i][j].resetPos(false);
      tiles_[i][j].flipOver();
    }
  }
}

void Game::genNewRoundTime() {
  if (settings_->game_getTimerEnabled()) {
    switch (settings_->game_getDifficulty()) {
      case Settings::GameDifficulty::EASY:            { game_timeLeft_ = 10; } break;
      case Settings::GameDifficulty::MEDIUM: default: { game_timeLeft_ = 6; } break;
      case Settings::GameDifficulty::HARD:            { game_timeLeft_ = 5; } break;
      case Settings::GameDifficulty::INSANE:          { game_timeLeft_ = 4; } break;
      case Settings::GameDifficulty::UNFAIR:          { game_timeLeft_ = 4; } break;
    }
  } else {
    game_timeLeft_ = 0;
  }
}

void Game::pressedButton(const SelectedGroup selectedGroup, const bool forceWrongAnswer) {
  if (
    (state_ != State::RUNNING) ||
    (round_currentStep_ > tiles_.size() - 1)
  ) {
    return;
  }

  const bool correctAnswer = forceWrongAnswer ? false : (round_currentOrder_[round_currentStep_] == static_cast<unsigned int>(selectedGroup));
  ++round_currentStep_;

  bool nextRound = forceWrongAnswer ? true : false;

  if (nextRound) {
    ++game_numAnswersWrong_;
  } else {
    // wrong answer
    if (!correctAnswer) {
      ++game_numAnswersWrong_;
      nextRound = true;
    } 
    // max step reached / correct answer
    else if (round_currentStep_ > tiles_.size() - 1) {
      ++game_numAnswersCorrect_;
      nextRound = true;
    } 
    // correct answer but not done yet
    else {
      deactivateTiles(static_cast<unsigned int>(selectedGroup));
    }
  }

  if (forceWrongAnswer || !correctAnswer) {
    flashBackground(EventType::WRONG_ANSWER);
    shakeCamera_[static_cast<int>(Camera::ShakeType::WRONG_ANSWER)] = true;
  }

  if (nextRound) {
    ++game_currentRound_;
    if (game_currentRound_ > settings_->game_getRoundsAsValue()) {
      game_currentRound_ = settings_->game_getRoundsAsValue();
      // flip over the last group if the answer was correct
      if (correctAnswer) {
        deactivateTiles(static_cast<unsigned int>(selectedGroup));
      }
      state_ = State::FINISHED;
      soundSystem_->playSound(Resources::Sound::FINISHED);
    } else {
      if (correctAnswer) {
        soundSystem_->playSound(Resources::Sound::ANSWER_CORRECT);
      } else {
        soundSystem_->playSound(Resources::Sound::ANSWER_INCORRECT);
      }
      genNewRoundTime();
      game_timerAccum_ = 0;
      generateRound();
    }
    updateGameInfo(false, true, true, settings_->game_getTimerEnabled());
  } else {
    soundSystem_->playSound(Resources::Sound::ANSWER_CORRECT);
  }
}

void Game::flashBackground(const EventType eventType) {
  switch (eventType) {
    case EventType::NONE: default: { return; } break;
    case EventType::WRONG_ANSWER:  { flashColor_ = sf::Color(70, 0, 0); } break;
  }

  flashAlpha_ = 255;
  flashColor_.a = flashAlpha_;
  flashRect_.setFillColor(flashColor_);
}

void Game::deactivateTiles(const unsigned int groupIndex) {
  for (std::size_t i = 0; i < tiles_[groupIndex].size(); ++i) {
    if (tiles_[groupIndex][i].getType() != Tile::Type::INVIS) {
      tiles_[groupIndex][i].setType(Tile::Type::NEUTRAL);
      tiles_[groupIndex][i].flipOver();
    }
  }
}

void Game::menu_select() {
  mousePos_ = window_->mapPixelToCoords(sf::Mouse::getPosition(*window_));

  bool process = false;
  std::size_t index = 0;
  bool rightButtonSide = true;

  switch (state_) {

    case State::TITLE:
      {
        for (std::size_t i = 0; i < titleRects_.size(); ++i) {
          if (titleRects_[i].getGlobalBounds().contains(mousePos_)) {
            process = true;
            index = i;
            rightButtonSide = mousePos_.x >= (titleRects_[i].getPosition().x + (titleRects_[i].getSize().x / 2));
            break;
          }
        }
        if (!process) {
          const std::size_t maxIndex = menu_settings_showApplyButton_ ? settingsRects_.size() : settingsRects_.size() - 1;
          for (std::size_t i = 0; i < maxIndex; ++i) {
            if (settingsRects_[i].getGlobalBounds().contains(mousePos_)) {
              process = true;
              index = i + titleRects_.size();
              rightButtonSide = mousePos_.x >= (settingsRects_[i].getPosition().x + (settingsRects_[i].getSize().x / 2));
              break;
            }
          }
        }
      }
      break;

    case State::HELP:
    case State::ABOUT:
      {
        process = true;
      }
      break;

    case State::RUNNING:
      {
      }
      break;

    case State::PAUSED:
      {
        for (std::size_t i = 0; i < pauseRects_.size(); ++i) {
          if (pauseRects_[i].getGlobalBounds().contains(mousePos_)) {
            process = true;
            index = i;
            break;
          }
        }
        if (!process) {
          const std::size_t maxIndex = menu_settings_showApplyButton_ ? settingsRects_.size() : settingsRects_.size() - 1;
          for (std::size_t i = 0; i < maxIndex; ++i) {
            if (settingsRects_[i].getGlobalBounds().contains(mousePos_)) {
              process = true;
              index = i + pauseRects_.size();
              rightButtonSide = mousePos_.x >= (settingsRects_[i].getPosition().x + (settingsRects_[i].getSize().x / 2));
              break;
            }
          }
        }
      }
      break;

    case State::FINISHED:
      {
        for (std::size_t i = 0; i < finishedRects_.size(); ++i) {
          if (finishedRects_[i].getGlobalBounds().contains(mousePos_)) {
            process = true;
            index = i;
            break;
          }
        }
      }
      break;

    default:
      {
      }
      break;

  }

  if (process) {
    menu_processAction(index, rightButtonSide);
  }
}

void Game::menu_processAction(const std::size_t index, const bool rightButtonSide) {
  switch (state_) {

    case State::TITLE:
      {
        switch (index) {
          case 0:  { menu_title_difficulty(rightButtonSide); } break;
          case 1:  { menu_title_rounds(rightButtonSide); } break;
          case 2:  { menu_title_timer(rightButtonSide); } break;
          case 3:  { initGame(); } break;
          case 4:  { state_ = State::HELP; menuSelectionRectDraw_ = false; soundSystem_->playSound(Resources::Sound::MENU_SELECT); } break;
          case 5:  { state_ = State::ABOUT; menuSelectionRectDraw_ = false; soundSystem_->playSound(Resources::Sound::MENU_SELECT); } break;
          case 6:  { exitProgram(); } break;
          case 7:  { menu_settings_fps(rightButtonSide); } break;
          case 8:  { menu_settings_fpsValue(rightButtonSide); } break;
          case 9:  { menu_settings_vsync(rightButtonSide); } break;
          case 10: { menu_settings_sfx(rightButtonSide); } break;
          case 11: { menu_settings_sfxVol(rightButtonSide); } break;
          case 12: { menu_settings_applyVideo(); } break;
          default: {} break;
        }
      }
      break;

    case State::HELP:
    case State::ABOUT:
      {
        state_ = State::TITLE;
        soundSystem_->playSound(Resources::Sound::MENU_SELECT);
      }
      break;

    case State::RUNNING:
      {
      }
      break;

    case State::PAUSED:
      {
        switch (index) {
          case 0:  { resumeGame(); } break;
          case 1:  { restartGame(); } break;
          case 2:  { quitGame(); } break;
          case 3:  { menu_settings_fps(rightButtonSide); } break;
          case 4:  { menu_settings_fpsValue(rightButtonSide); } break;
          case 5:  { menu_settings_vsync(rightButtonSide); } break;
          case 6:  { menu_settings_sfx(rightButtonSide); } break;
          case 7:  { menu_settings_sfxVol(rightButtonSide); } break;
          case 8:  { menu_settings_applyVideo(); } break;
          default: {} break;
        }
      }
      break;

    case State::FINISHED:
      {
        switch (index) {
          case 0:  { restartGame(); } break;
          case 1:  { quitGame(); } break;
          default: {} break;
        }
      }
      break;

    default:
      {
      }
      break;

  }
}

void Game::menu_title_difficulty(const bool increase) {
  bool update = false;
  const Settings::GameDifficulty gameDifficulty = settings_->game_getDifficulty();

  if (increase) {
    if (gameDifficulty < static_cast<Settings::GameDifficulty>(static_cast<int>(Settings::GameDifficulty::NUMBER_OF_DIFFICULTIES) - 1)) {
      settings_->game_setDifficulty(static_cast<Settings::GameDifficulty>(static_cast<int>(gameDifficulty) + 1));
      update = true;
    }
  } else {
    if (gameDifficulty > static_cast<Settings::GameDifficulty>(0)) {
      settings_->game_setDifficulty(static_cast<Settings::GameDifficulty>(static_cast<int>(gameDifficulty) - 1));
      update = true;
    }
  }

  if (update) {
    soundSystem_->playSound(Resources::Sound::MENU_SELECT);
    titleValuesTexts_[0].setString(settings_->game_getDifficultyAsString());
    menu_title_updateTextsPos();
  }
}

void Game::menu_title_rounds(const bool increase) {
  bool update = false;
  const Settings::GameRounds gameRounds = settings_->game_getRounds();

  if (increase) {
    if (gameRounds < static_cast<Settings::GameRounds>(static_cast<int>(Settings::GameRounds::NUMBER_OF_ROUNDS) - 1)) {
      settings_->game_setRounds(static_cast<Settings::GameRounds>(static_cast<int>(gameRounds) + 1));
      update = true;
    }
  } else {
    if (gameRounds > static_cast<Settings::GameRounds>(0)) {
      settings_->game_setRounds(static_cast<Settings::GameRounds>(static_cast<int>(gameRounds) - 1));
      update = true;
    }
  }

  if (update) {
    soundSystem_->playSound(Resources::Sound::MENU_SELECT);
    titleValuesTexts_[1].setString(std::to_string(settings_->game_getRoundsAsValue()));
    menu_title_updateTextsPos();
  }
}

void Game::menu_title_timer(const bool status) {
  if (settings_->game_getTimerEnabled() == status) {
    return;
  }

  soundSystem_->playSound(Resources::Sound::MENU_SELECT);
  settings_->game_setTimerEnabled(status);
  titleValuesTexts_[2].setString(settings_->game_getTimerEnabled() ? "on" : "off");
  menu_title_updateTextsPos();
}

void Game::menu_title_updateTextsPos() {
  for (std::size_t i = 0; i < titleValuesTexts_.size(); ++i) {
    const sf::FloatRect rectBounds = titleRects_[i].getGlobalBounds();
    titleValuesTexts_[i].setPos(sf::Vector2f(
      static_cast<float>(static_cast<int>(rectBounds.left + rectBounds.width / 2 - titleValuesTexts_[i].getWidth() / 2)), 
      static_cast<float>(static_cast<int>(rectBounds.top + rectBounds.height / 2 - titleValuesTexts_[i].getHeight() / 2))
    ));
  }
}

void Game::menu_settings_fps(const bool status) {
  if (settings_->getFpsLimited() == status) {
    return;
  }

  soundSystem_->playSound(Resources::Sound::MENU_SELECT);
  menu_settings_showApplyButton_ = true;
  settings_->setFpsLimited(status);
  settingsValuesTexts_[0].setString(settings_->getFpsLimited() ? "on" : "off");
  settingsValuesTexts_[1].setString(settings_->getFpsLimited() ? std::to_string(settings_->getFps()) : "-");
  settingsValuesTexts_[2].setString(settings_->getFpsLimited() ? "-" : (settings_->getVsync() ? "on" : "off"));
  menu_settings_updateTextsPos();
}

void Game::menu_settings_fpsValue(const bool increase) {
  if (!settings_->getFpsLimited()) {
    return;
  }

  bool update = false;
  const int step = 10;
  int val = static_cast<int>(settings_->getFps());

  if (increase) {
    const int fpsMax = static_cast<int>(settings_->getFpsMaximum());
    if (val < fpsMax) {
      val = (val + step >= fpsMax) ? fpsMax : (val + step);
      update = true;
    }
  } else {
    const int fpsMin = static_cast<int>(settings_->getFpsMinimum());
    if (val > fpsMin) {
      val = (val - step <= fpsMin) ? fpsMin : (val - step);
      update = true;
    }
  }

  if (update) {
    soundSystem_->playSound(Resources::Sound::MENU_SELECT);
    menu_settings_showApplyButton_ = true;
    settings_->setFps(static_cast<unsigned int>(val));
    settingsValuesTexts_[1].setString(settings_->getFpsLimited() ? std::to_string(settings_->getFps()) : "-");
    menu_settings_updateTextsPos();
  }
}

void Game::menu_settings_vsync(const bool status) {
  if (settings_->getFpsLimited() || (settings_->getVsync() == status)
  ) {
    return;
  }

  soundSystem_->playSound(Resources::Sound::MENU_SELECT);
  menu_settings_showApplyButton_ = true;
  settings_->setVsync(status);
  settingsValuesTexts_[2].setString(settings_->getFpsLimited() ? "-" : (settings_->getVsync() ? "on" : "off"));
  menu_settings_updateTextsPos();
}

void Game::menu_settings_sfx(const bool status) {
  if (settings_->getSfxEnabled() == status) {
    return;
  }

  if (!status) {
    soundSystem_->playSound(Resources::Sound::MENU_SELECT);
  }
  settings_->setSfxEnabled(status);
  soundSystem_->setSfxEnabled(settings_->getSfxEnabled());
  if (status) {
    soundSystem_->playSound(Resources::Sound::MENU_SELECT);
  }
  settingsValuesTexts_[3].setString(settings_->getSfxEnabled() ? "on" : "off");
  menu_settings_updateTextsPos();
}

void Game::menu_settings_sfxVol(const bool increase) {
  bool update = false;
  const int step = 5;
  int val = static_cast<int>(settings_->getSfxVolume());

  if (increase) {
    if (val < 100) {
      val = (val + step >= 100) ? 100 : (val + step);
      update = true;
    }
  } else {
    if (val > 0) {
      val = (val - step <= 0) ? 0 : (val - step);
      update = true;
    }
  }

  if (update) {
    if (!increase) {
      soundSystem_->playSound(Resources::Sound::MENU_SELECT);
    }
    settings_->setSfxVolume(static_cast<unsigned int>(val));
    soundSystem_->setSfxVolume(settings_->getSfxVolume());
    if (increase) {
      soundSystem_->playSound(Resources::Sound::MENU_SELECT);
    }
    settingsValuesTexts_[4].setString(std::to_string(settings_->getSfxVolume()));
    menu_settings_updateTextsPos();
  }
}

void Game::menu_settings_updateTextsPos() {
  for (std::size_t i = 0; i < settingsValuesTexts_.size(); ++i) {
    const sf::FloatRect rectBounds = settingsRects_[i].getGlobalBounds();
    settingsValuesTexts_[i].setPos(sf::Vector2f(
      static_cast<float>(static_cast<int>(rectBounds.left + rectBounds.width / 2 - settingsValuesTexts_[i].getWidth() / 2)), 
      static_cast<float>(static_cast<int>(rectBounds.top + rectBounds.height / 2 - settingsValuesTexts_[i].getHeight() / 2))
    ));
  }
}

void Game::menu_settings_applyVideo() {
  soundSystem_->playSound(Resources::Sound::MENU_CONFIRM);
  menu_settings_showApplyButton_ = false;

  window_->setFramerateLimit(settings_->getFpsLimited() ? settings_->getFps() : 0);
  window_->setVerticalSyncEnabled(settings_->getFpsLimited() ? false : settings_->getVsync());
}

void Game::exitProgram() {
  window_->close();
}