#include <algorithm>
#include <array>
#include <fstream>
#include <sstream>

#include "settings.h"
#include "utilities.h"

Settings::Settings() :
  file_options_("./fltm_settings"),

  videoMode_(sf::VideoMode(800, 800)),
  windowIsFocused_(true),
  fpsLimited_(true),
  fps_(60),
  vsync_(false),
  sfxEnabled_(true),
  sfxVolume_(20),

  game_difficulty_(GameDifficulty::MEDIUM),
  game_rounds_(GameRounds::FIFTEEN),
  game_timerEnabled_(true)
{
}

Settings::~Settings() {
}

const sf::VideoMode &Settings::getVideoMode() const {
  return videoMode_;
}

const sf::FloatRect Settings::getWindowRect() const {
  return sf::FloatRect(sf::Vector2f(0.0f, 0.0f), getWindowSize());
}

const sf::Vector2f Settings::getWindowSize() const {
  return sf::Vector2f(static_cast<float>(videoMode_.width), static_cast<float>(videoMode_.height));
}

const bool &Settings::getWindowIsFocused() const {
  return windowIsFocused_;
}

const bool &Settings::getFpsLimited() const {
  return fpsLimited_;
}

const unsigned int &Settings::getFps() const {
  return fps_;
}

const unsigned int Settings::getFpsMinimum() const {
  return 30;
}

const unsigned int Settings::getFpsMaximum() const {
  return 240;
}

const bool &Settings::getVsync() const {
  return vsync_;
}

const bool &Settings::getSfxEnabled() const {
  return sfxEnabled_;
}

const unsigned int &Settings::getSfxVolume() const {
  return sfxVolume_;
}

void Settings::setWindowIsFocused(const bool status) {
  windowIsFocused_ = status;
}

void Settings::setFpsLimited(const bool status) {
  fpsLimited_ = status;
}

void Settings::setFps(const unsigned int no) {
  fps_ = utils::clamp(no, getFpsMinimum(), getFpsMaximum());
}

void Settings::setVsync(const bool status) {
  vsync_ = status;
}

void Settings::setSfxEnabled(const bool status) {
  sfxEnabled_ = status;
}

void Settings::setSfxVolume(const unsigned int no) {
  sfxVolume_ = utils::clamp(no, 0U, 100U);
}

const Settings::GameDifficulty &Settings::game_getDifficulty() const {
  return game_difficulty_;
}

const std::string Settings::game_getDifficultyAsString() const {
  switch (game_difficulty_) {
    case GameDifficulty::EASY:            { return "easy"; } break;
    case GameDifficulty::MEDIUM: default: { return "normal"; } break;
    case GameDifficulty::HARD:            { return "hard"; } break;
    case GameDifficulty::INSANE:          { return "insane"; } break;
    case GameDifficulty::UNFAIR:          { return "unfair"; } break;
  }
}

const Settings::GameRounds &Settings::game_getRounds() const {
  return game_rounds_;
}

const unsigned int Settings::game_getRoundsAsValue() const {
  switch (game_rounds_) {
    case GameRounds::FIVE:             { return 5; } break;
    case GameRounds::TEN:              { return 10; } break;
    case GameRounds::FIFTEEN: default: { return 15; } break;
    case GameRounds::TWENTY:           { return 20; } break;
    case GameRounds::TWENTYFIVE:       { return 25; } break;
    case GameRounds::THIRTY:           { return 30; } break;
    case GameRounds::THIRTYFIVE:       { return 35; } break;
    case GameRounds::FORTY:            { return 40; } break;
    case GameRounds::FORTYFIVE:        { return 45; } break;
    case GameRounds::FIFTY:            { return 50; } break;
  }
}

const bool &Settings::game_getTimerEnabled() const {
  return game_timerEnabled_;
}

void Settings::game_setDifficulty(const GameDifficulty val) {
  if (
    (val >= static_cast<GameDifficulty>(0)) &&
    (val < GameDifficulty::NUMBER_OF_DIFFICULTIES)
  ) {
    game_difficulty_ = val;
  }
}

void Settings::game_setRounds(const GameRounds val) {
  if (
    (val >= static_cast<GameRounds>(0)) &&
    (val < GameRounds::NUMBER_OF_ROUNDS)
  ) {
    game_rounds_ = val;
  }
}

void Settings::game_setTimerEnabled(const bool status) {
  game_timerEnabled_ = status;
}

void Settings::saveOptionsToFile() {
  std::stringstream ss;
  std::ofstream ofile(file_options_);

  if (ofile.good()) {
    ofile.clear();
    ss << "limitfps=" << (getFpsLimited() ? "1" : "0") << std::endl;
    ss << "fps=" << getFps() << std::endl;
    ss << "vsync=" << (getVsync() ? "1" : "0") << std::endl;
    ss << "sfx=" << (getSfxEnabled() ? "1" : "0") << std::endl;
    ss << "sfxvol=" << getSfxVolume() << std::endl;
    ss << "gamedifficulty=" << static_cast<unsigned int>(game_difficulty_) << std::endl;
    ss << "gamerounds=" << static_cast<unsigned int>(game_rounds_) << std::endl;
    ss << "gametimer=" << (game_getTimerEnabled() ? "1" : "0") << std::endl;
    ofile << ss.str();
    ofile.close();
  }
}

void Settings::loadOptionsFromFile() {
  std::ifstream ifile(file_options_);

  if (ifile.good()) {
    const std::string content((std::istreambuf_iterator<char>(ifile)), (std::istreambuf_iterator<char>()));
    std::istringstream split(content);
    std::vector<std::string> tokens;
    for (std::string each; std::getline(split, each, '\n'); tokens.push_back(each));

    for (std::size_t i = 0; i < tokens.size(); ++i) {
      const std::size_t pos = tokens[i].find("=");
      if (pos == std::string::npos) { continue; }
      const std::string key = tokens[i].substr(0, pos);
      const std::string valueString = tokens[i].substr(pos + 1);
      const unsigned int valueMaxLength = 5;
      if ((valueString.length() < 1) || (valueString.length() > valueMaxLength)) { continue; }
      const int value = std::atoi(tokens[i].substr(pos + 1).c_str());
      if (value < 0) { continue; }
      {
        if (key == "limitfps")            { setFpsLimited(value == 1); }
        else if (key == "fps")            { setFps(value); }
        else if (key == "vsync")          { setVsync(value == 1); }
        else if (key == "sfx")            { setSfxEnabled(value == 1); }
        else if (key == "sfxvol")         { setSfxVolume(value); }
        else if (key == "gamedifficulty") { game_setDifficulty(static_cast<GameDifficulty>(value)); }
        else if (key == "gamerounds")     { game_setRounds(static_cast<GameRounds>(value)); }
        else if (key == "gametimer")      { game_setTimerEnabled(value == 1); }
      }
    }
  }
}