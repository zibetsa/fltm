#ifndef TEXT_H_INCLUDED
#define TEXT_H_INCLUDED

#include <SFML/Graphics.hpp>

class Text : public sf::Drawable {
 public:
  enum class Font {
    DEFAULT
  };

  Text();
  Text(sf::Vector2f pos, const std::string str, Font font, sf::Texture &texture, float scale = 1.0f, sf::Color color = sf::Color(0, 0, 0));
  virtual ~Text();

  const sf::Vector2f getPos() const;
  const sf::FloatRect &getBounds() const;
  const std::string &getString() const;
  const float getWidth() const;
  const float getHeight() const;
  const int getAlpha() const;

  void setPos(const sf::Vector2f &pos);
  void setString(const std::string str);
  void setColor(const sf::Color &color);
  void setAlpha(const int &alpha);
  void setScale(const float scale);

 private:
  enum class Char {
    CHAR_UNKNOWN,

    CHAR_A,
    CHAR_B,
    CHAR_C,
    CHAR_D,
    CHAR_E,
    CHAR_F,
    CHAR_G,
    CHAR_H,
    CHAR_I,
    CHAR_J,
    CHAR_K,
    CHAR_L,
    CHAR_M,
    CHAR_N,
    CHAR_O,
    CHAR_P,
    CHAR_Q,
    CHAR_R,
    CHAR_S,
    CHAR_T,
    CHAR_U,
    CHAR_V,
    CHAR_W,
    CHAR_X,
    CHAR_Y,
    CHAR_Z,

    CHAR_A_CAPITAL,
    CHAR_B_CAPITAL,
    CHAR_C_CAPITAL,
    CHAR_D_CAPITAL,
    CHAR_E_CAPITAL,
    CHAR_F_CAPITAL,
    CHAR_G_CAPITAL,
    CHAR_H_CAPITAL,
    CHAR_I_CAPITAL,
    CHAR_J_CAPITAL,
    CHAR_K_CAPITAL,
    CHAR_L_CAPITAL,
    CHAR_M_CAPITAL,
    CHAR_N_CAPITAL,
    CHAR_O_CAPITAL,
    CHAR_P_CAPITAL,
    CHAR_Q_CAPITAL,
    CHAR_R_CAPITAL,
    CHAR_S_CAPITAL,
    CHAR_T_CAPITAL,
    CHAR_U_CAPITAL,
    CHAR_V_CAPITAL,
    CHAR_W_CAPITAL,
    CHAR_X_CAPITAL,
    CHAR_Y_CAPITAL,
    CHAR_Z_CAPITAL,

    CHAR_1,
    CHAR_2,
    CHAR_3,
    CHAR_4,
    CHAR_5,
    CHAR_6,
    CHAR_7,
    CHAR_8,
    CHAR_9,
    CHAR_0,

    CHAR_SPACE,
    CHAR_PLUS,
    CHAR_MINUS,
    CHAR_MULTIPLY,
    CHAR_DIVIDE,
    CHAR_EQUALS,
    CHAR_NUMBER_SIGN,
    CHAR_AMPERSAND,
    CHAR_UNDERSCORE,
    CHAR_PARANTHESIS_LEFT,
    CHAR_PARANTHESIS_RIGHT,
    CHAR_BRACKET_LEFT,
    CHAR_BRACKET_RIGHT,
    CHAR_BIG_BRACKET_LEFT,
    CHAR_BIG_BRACKET_RIGHT,
    CHAR_LESS_THAN,
    CHAR_GREATER_THAN,
    CHAR_COMMA,
    CHAR_FULL_STOP,
    CHAR_SEMICOLON,
    CHAR_COLON,
    CHAR_QUESTION_MARK,
    CHAR_EXCLAMATION_MARK,
    CHAR_APOSTROPHE,
    CHAR_AT,
    CHAR_INFINITY
  };
  
  virtual void draw(sf::RenderTarget &renderTarget, sf::RenderStates renderStates) const;
  Char charToEnum(const char myChar) const;

  bool created_;
  Font font_;
  sf::FloatRect bounds_;
  sf::Texture *texture_;
  sf::Vector2f position_;
  std::string string_;
  float scale_;
  sf::Color color_;

  std::vector<sf::Sprite> chars_;

};

#endif