#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

#include <map>
#include <string>

#include <SFML/Graphics.hpp>

class Settings {
 public:
  enum class GameDifficulty {
    EASY,
    MEDIUM,
    HARD,
    INSANE,
    UNFAIR,

    NUMBER_OF_DIFFICULTIES
  };

  enum class GameRounds {
    FIVE,
    TEN,
    FIFTEEN,
    TWENTY,
    TWENTYFIVE,
    THIRTY,
    THIRTYFIVE,
    FORTY,
    FORTYFIVE,
    FIFTY,

    NUMBER_OF_ROUNDS
  };

  Settings();
  virtual ~Settings();
  
  const sf::VideoMode &getVideoMode() const;
  const sf::FloatRect getWindowRect() const;
  const sf::Vector2f getWindowSize() const;
  const bool &getWindowIsFocused() const;
  const bool &getFpsLimited() const;
  const unsigned int &getFps() const;
  const unsigned int getFpsMinimum() const;
  const unsigned int getFpsMaximum() const;
  const bool &getVsync() const;
  const bool &getSfxEnabled() const;
  const unsigned int &getSfxVolume() const;
  void setWindowIsFocused(const bool status);
  void setFpsLimited(const bool status);
  void setFps(const unsigned int no);
  void setVsync(const bool status);
  void setSfxEnabled(const bool status);
  void setSfxVolume(const unsigned int no);

  const GameDifficulty &game_getDifficulty() const;
  const std::string game_getDifficultyAsString() const;
  const GameRounds &game_getRounds() const;
  const unsigned int game_getRoundsAsValue() const;
  const bool &game_getTimerEnabled() const;
  void game_setDifficulty(const GameDifficulty val);
  void game_setRounds(const GameRounds val);
  void game_setTimerEnabled(const bool status);
  
  void saveOptionsToFile();
  void loadOptionsFromFile();

 private:
  void defaultSettings();

  const std::string file_options_;

  sf::VideoMode videoMode_;
  bool windowIsFocused_;
  bool fpsLimited_;
  unsigned int fps_;
  bool vsync_;
  bool sfxEnabled_;
  unsigned int sfxVolume_;

  GameDifficulty game_difficulty_;
  GameRounds game_rounds_;
  bool game_timerEnabled_;

};

#endif