#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED

#include <SFML/Graphics.hpp>

class Tile : public sf::Drawable {
 public:
  enum class Type {
    YELLOW,
    RED,
    GREEN,
    BLUE,
    
    NUMBER_OF_TYPES,

    INVIS,
    NEUTRAL
  };

  Tile();
  Tile(const sf::Vector2f pos, const sf::Texture &texture, const sf::FloatRect viewRect);
  virtual ~Tile();

  const Type &getType() const;

  void resetPos(const bool moveOut);
  void update(sf::Time dt);
  void setType(const Type type);
  void flipOver();

 private:
  void init();
  void updateTextureRect();
  void makeOtherSideInvis();
  const sf::IntRect getTexRect(Type type) const;
  void updateSpriteScale();
  virtual void draw(sf::RenderTarget &renderTarget, sf::RenderStates renderStates) const;

  sf::FloatRect viewRect_;
  Type type_;
  bool movingOut_;
  sf::Vector2f posOrig_;
  sf::Vector2f pos_;
  sf::Vector2f posDest_;
  float springSpeed_;
  sf::Vector2f springVel_;

  int timeBeforeSpinning_;
  bool updateFlipping_;
  int flipDirection_ ; // -1: hide, 0: don't spin, 1: show
  bool showFront_;
  sf::Time flipTime_;
  sf::Time halfFlipTime_;
  sf::Time curFlipTime_;
  float scale_;
  float scaleFactor_;

  sf::RectangleShape spriteFront_;
  sf::RectangleShape spriteBack_;

};

#endif