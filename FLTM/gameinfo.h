#ifndef GAMEINFO_H_INCLUDED
#define GAMEINFO_H_INCLUDED

#include <string>

namespace gameinfo {

const std::string NAME = "FLTM";
const std::string NAME_ADV = "From Least To Most";
const std::string VERSION = "0.1.0.0";
const std::string DATE = "04 Feb 2017";
const std::string AUTHOR = "Axel Meyer";
const std::string WEBSITE = "www.axel-m.de";

}

#endif