#include "resources.h"
#include "res_sounds.h"
#include "res_textures.h"

Resources::Resources() {
  loadAllTextures();
  loadAllSounds();
}

Resources::~Resources() {
}

const sf::Uint8 *Resources::getAppIconData() {
  return RES_APPICON.pixel_data;
}

sf::Texture &Resources::getTexture(const Texture texture) {
  switch (texture) {
    case Texture::DUMMY:
    default:
      return texture_dummy_;
    case Texture::FONT:
      return texture_font_;
    case Texture::TILES:
      return texture_tiles_;
  }
}

sf::SoundBuffer &Resources::getSoundBuffer(const Sound sound) {
  if (sounds_.count(sound) > 0) {
    return sounds_.at(sound);
  }

  return sound_dummy_;
}

std::unique_ptr<sf::Shader> Resources::getShader_gaussianBlur() {
  auto shader = std::unique_ptr<sf::Shader>(new sf::Shader());

  const std::string fragmentShaderSource = \
    "uniform sampler2D sourceTexture;"\
    "uniform vec2 offsetFactor;"\
    "void main() {"\
	  "  vec2 textureCoordinates = gl_TexCoord[0].xy;"\
	  "  vec4 color = vec4(0.0);"\
	  "  color += texture2D(sourceTexture, textureCoordinates - 4.0 * offsetFactor) * 0.0162162162;"\
	  "  color += texture2D(sourceTexture, textureCoordinates - 3.0 * offsetFactor) * 0.0540540541;"\
	  "  color += texture2D(sourceTexture, textureCoordinates - 2.0 * offsetFactor) * 0.1216216216;"\
	  "  color += texture2D(sourceTexture, textureCoordinates - offsetFactor) * 0.1945945946;"\
	  "  color += texture2D(sourceTexture, textureCoordinates) * 0.2270270270;"\
	  "  color += texture2D(sourceTexture, textureCoordinates + offsetFactor) * 0.1945945946;"\
	  "  color += texture2D(sourceTexture, textureCoordinates + 2.0 * offsetFactor) * 0.1216216216;"\
	  "  color += texture2D(sourceTexture, textureCoordinates + 3.0 * offsetFactor) * 0.0540540541;"\
	  "  color += texture2D(sourceTexture, textureCoordinates + 4.0 * offsetFactor) * 0.0162162162;"\
	  "  gl_FragColor = color;"\
    "}";

  shader->loadFromMemory(fragmentShaderSource, sf::Shader::Type::Fragment);

  return shader;
}

void Resources::loadAllTextures() {
#ifdef _DEBUG
  texture_dummy_.loadFromMemory(RES_TEXTURE_DUMMY, sizeof(RES_TEXTURE_DUMMY));
  texture_font_.loadFromFile("./data/tex_font.png");
  texture_tiles_.loadFromFile("./data/tex_tiles.png");
#else
  texture_dummy_.loadFromMemory(RES_TEXTURE_DUMMY, sizeof(RES_TEXTURE_DUMMY));
  texture_font_.loadFromMemory(RES_TEXTURE_FONT, sizeof(RES_TEXTURE_FONT));
  texture_tiles_.loadFromMemory(RES_TEXTURE_TILES, sizeof(RES_TEXTURE_TILES));
#endif
}

void Resources::loadAllSounds() {
  bool okay = true;

#ifdef _DEBUG
  std::vector<std::string> paths;
  paths.resize(static_cast<unsigned int>(Sound::NUMBER_OF_SOUNDS));
  {
    paths[static_cast<std::size_t>(Sound::MENU_SELECT)] = "sfx_menu_select";
    paths[static_cast<std::size_t>(Sound::MENU_CONFIRM)] = "sfx_menu_confirm";
    paths[static_cast<std::size_t>(Sound::PAUSE)] = "sfx_pause";
    paths[static_cast<std::size_t>(Sound::ANSWER_CORRECT)] = "sfx_answer_correct";
    paths[static_cast<std::size_t>(Sound::ANSWER_INCORRECT)] = "sfx_answer_incorrect";
    paths[static_cast<std::size_t>(Sound::FINISHED)] = "sfx_finished";
  }
#endif

  for (std::size_t i = 0; i < static_cast<std::size_t>(Sound::NUMBER_OF_SOUNDS); ++i) {
#ifdef _DEBUG
    okay = sounds_[static_cast<Sound>(i)].loadFromFile("./data/" + paths[i] + ".ogg");
#else
    void *buf = nullptr;
    size_t siz = 0;
    switch (static_cast<Sound>(i)) {
      case Sound::MENU_SELECT:      { buf = RES_SOUND_MENU_SELECT; siz = sizeof(RES_SOUND_MENU_SELECT); } break;
      case Sound::MENU_CONFIRM:     { buf = RES_SOUND_MENU_CONFIRM; siz = sizeof(RES_SOUND_MENU_CONFIRM); } break;
      case Sound::PAUSE:            { buf = RES_SOUND_PAUSE; siz = sizeof(RES_SOUND_PAUSE); } break;
      case Sound::ANSWER_CORRECT:   { buf = RES_SOUND_ANSWER_CORRECT; siz = sizeof(RES_SOUND_ANSWER_CORRECT); } break;
      case Sound::ANSWER_INCORRECT: { buf = RES_SOUND_ANSWER_INCORRECT; siz = sizeof(RES_SOUND_ANSWER_INCORRECT); } break;
      case Sound::FINISHED:         { buf = RES_SOUND_FINISHED; siz = sizeof(RES_SOUND_FINISHED); } break;
      default:                      { buf = RES_SOUND_DUMMY; siz = sizeof(RES_SOUND_DUMMY); } break;
    }
    okay = (buf != nullptr) && sounds_[static_cast<Sound>(i)].loadFromMemory(buf, siz);
#endif
    if (!okay) {
      break;
    }
  }

  sound_dummy_.loadFromMemory(RES_SOUND_DUMMY, sizeof(RES_SOUND_DUMMY));
}