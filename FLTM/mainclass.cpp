#include <cstdlib>

#include "game.h"
#include "gameinfo.h"
#include "mainclass.h"

MainClass::MainClass() {
}

MainClass::~MainClass() {
}

int MainClass::operator()() {
  std::srand(static_cast<unsigned int>(time(0)));

#ifdef NDEBUG
  settings_.loadOptionsFromFile();
#endif

  soundSystem_.init(&resources_);
  soundSystem_.setSfxEnabled(settings_.getSfxEnabled());
  soundSystem_.setSfxVolume(settings_.getSfxVolume());

  window_ = std::unique_ptr<sf::RenderWindow>(new sf::RenderWindow(settings_.getVideoMode(), gameinfo::NAME, sf::Style::Close));
  window_->setIcon(32, 32, resources_.getAppIconData());
  window_->setFramerateLimit(settings_.getFpsLimited() ? settings_.getFps() : 0);
  window_->setVerticalSyncEnabled(settings_.getFpsLimited() ? false : settings_.getVsync());
  window_->setKeyRepeatEnabled(false);
  window_->setMouseCursorVisible(true);
  window_->setMouseCursorGrabbed(false);

  Game game(window_.get(), &resources_, &settings_, &soundSystem_);

  sf::Clock deltaClock;
  const sf::Time timePerFrame = sf::seconds(1.0f / 60.0f);
  sf::Time timeSinceLastUpdate = sf::Time::Zero;

  while (window_->isOpen()) {
    timeSinceLastUpdate += deltaClock.restart();
    game.processEvents();
    while (timeSinceLastUpdate > timePerFrame) {
      timeSinceLastUpdate -= timePerFrame;
      game.update(timePerFrame);
    }
    game.render();
  }
  
#ifdef NDEBUG
  settings_.saveOptionsToFile();
#endif

  soundSystem_.stopSfx();

  return EXIT_SUCCESS;
}