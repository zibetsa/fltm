#include "tile.h"
#include "utilities.h"

Tile::Tile() {
  init();
}

Tile::Tile(const sf::Vector2f pos, const sf::Texture &texture, const sf::FloatRect viewRect) {
  init();
  
  viewRect_ = viewRect;
  posOrig_ = pos;
  pos_ = posOrig_;
  posDest_ = posOrig_;

  spriteFront_ = utils::createTexturedRect(posOrig_, sf::Vector2f(utils::getTileSize(), utils::getTileSize()), sf::Color(255, 255, 255), texture, getTexRect(Type::INVIS));
  spriteBack_ = utils::createTexturedRect(posOrig_, sf::Vector2f(utils::getTileSize(), utils::getTileSize()), sf::Color(255, 255, 255), texture, getTexRect(Type::INVIS));

  spriteFront_.setOrigin(spriteFront_.getGlobalBounds().width / 2.0f, spriteFront_.getGlobalBounds().height / 2.0f);
  spriteBack_.setOrigin(spriteBack_.getGlobalBounds().width / 2.0f, spriteBack_.getGlobalBounds().height / 2.0f);
}

Tile::~Tile() {
}

const Tile::Type &Tile::getType() const {
  return type_;
}

void Tile::resetPos(const bool moveOut) {
  if (moveOut) {
    /*
    const float addOff = utils::getRandomFloat(20.0f, 30.0f);
    switch (std::rand() % 4) {
      // top
      case 0: default: { posDest_ = sf::Vector2f(utils::getRandomFloat(viewRect_.left, viewRect_.left + viewRect_.width - utils::getTileSize()), viewRect_.top - utils::getTileSize() - addOff); } break;
      // right
      case 1:          { posDest_ = sf::Vector2f(viewRect_.left + viewRect_.width + addOff, utils::getRandomFloat(viewRect_.top, viewRect_.top + viewRect_.height - utils::getTileSize())); } break;
      // bottom
      case 2:          { posDest_ = sf::Vector2f(utils::getRandomFloat(viewRect_.left, viewRect_.left + viewRect_.width - utils::getTileSize()), viewRect_.top + viewRect_.height + addOff); } break;
      // left
      case 3:          { posDest_ = sf::Vector2f(viewRect_.left - utils::getTileSize() - addOff, utils::getRandomFloat(viewRect_.top, viewRect_.top + viewRect_.height - utils::getTileSize())); } break;
    }
    */
    posDest_ = sf::Vector2f(
      utils::getRandomFloat(viewRect_.left - utils::getTileSize() / 2, viewRect_.left + viewRect_.width - utils::getTileSize() / 2), 
      utils::getRandomFloat(viewRect_.top - utils::getTileSize() / 2, viewRect_.top + viewRect_.height - utils::getTileSize() / 2)
    );
  } else {
    posDest_ = posOrig_;
  }

  movingOut_ = moveOut;
  springSpeed_ = moveOut ? 3.0f : utils::getRandomFloat(16.0f, 18.0f);
  springVel_ = sf::Vector2f(0.0f, 0.0f);
}

void Tile::update(sf::Time dt) {
  {
    utils::spring(pos_.x, springVel_.x, posDest_.x, 0.8f, springSpeed_, dt.asSeconds());
    utils::spring(pos_.y, springVel_.y, posDest_.y, 0.8f, springSpeed_, dt.asSeconds());
    spriteFront_.setPosition(pos_);
    spriteBack_.setPosition(pos_);
  }

  if (updateFlipping_) {
    if (timeBeforeSpinning_ > 0) {
      timeBeforeSpinning_ -= dt.asMilliseconds();
    } else {
      bool spinDone = false;
      if (flipDirection_ > 0) {
        curFlipTime_ += dt;
        if (curFlipTime_ >= flipTime_ - dt) {
          curFlipTime_ = flipTime_;
          spinDone = true;
        }
      } else if (flipDirection_ < 0) {
        curFlipTime_ -= dt;
        if (curFlipTime_ <= dt) {
          curFlipTime_ = sf::Time::Zero;
          spinDone = true;
        }
      }
      if (spinDone) {
        timeBeforeSpinning_ = 0;
        updateFlipping_ = false;
        flipDirection_ = 0;
        makeOtherSideInvis();
      }
      updateSpriteScale();
    }
  }
}

void Tile::setType(const Type type) {
  if (type_ == type) {
    return;
  }

  type_ = type;
  updateTextureRect();
}

void Tile::flipOver() {
  if (updateFlipping_) {
    timeBeforeSpinning_ = 0;
    updateFlipping_ = false;
    flipDirection_ = 0;
    showFront_ = false;
    curFlipTime_ = sf::Time::Zero;
    scale_ = 1.0f;
    scaleFactor_ = 1.0f;
    spriteBack_.setTextureRect(getTexRect(Type::INVIS));
    spriteFront_.setTextureRect(getTexRect(Type::INVIS));
    updateTextureRect();
    updateSpriteScale();
  }

  flipDirection_ = showFront_ ? -1 : 1;
  updateFlipping_ = true;
  timeBeforeSpinning_ = utils::getRandomInt(0, 150);
}

void Tile::init() {
  viewRect_ = sf::FloatRect(0.0f, 0.0f, 0.0f, 0.0f);
  type_ = Type::INVIS;
  movingOut_ = false;
  posOrig_ = sf::Vector2f(0.0f, 0.0f);
  pos_ = sf::Vector2f(0.0f, 0.0f);
  posDest_ = sf::Vector2f(0.0f, 0.0f);
  springSpeed_ = 0.0f;
  springVel_ = sf::Vector2f(0.0f, 0.0f);

  timeBeforeSpinning_ = 0;
  updateFlipping_ = false;
  flipDirection_ = 0;
  showFront_ = false;
  flipTime_ = sf::seconds(0.1f);//sf::seconds(0.08f);
  halfFlipTime_ = sf::seconds(0.05f);//sf::seconds(0.04f);
  curFlipTime_ = sf::Time::Zero;
  scale_ = 1.0f;
  scaleFactor_ = 1.0f;
}

void Tile::updateTextureRect() {
  if (showFront_) {
    spriteBack_.setTextureRect(getTexRect(type_));
  } else {
    spriteFront_.setTextureRect(getTexRect(type_));
  }
}

void Tile::makeOtherSideInvis() {
  if (showFront_) {
    spriteBack_.setTextureRect(getTexRect(Type::INVIS));
  } else {
    spriteFront_.setTextureRect(getTexRect(Type::INVIS));
  }
}

const sf::IntRect Tile::getTexRect(Type type) const {
  const int tileSize = static_cast<int>(utils::getTileSize());
  switch (type) {
    case Type::YELLOW:         { return sf::IntRect(40, 0, tileSize, tileSize); } break;
    case Type::RED:            { return sf::IntRect(60, 0, tileSize, tileSize); } break;
    case Type::GREEN:          { return sf::IntRect(80, 0, tileSize, tileSize); } break;
    case Type::BLUE:           { return sf::IntRect(100, 0, tileSize, tileSize); } break;
    case Type::INVIS: default: { return sf::IntRect(0, 0, tileSize, tileSize); } break;
    case Type::NEUTRAL:        { return sf::IntRect(20, 0, tileSize, tileSize); } break;
  }
}

void Tile::updateSpriteScale() {
  showFront_ = curFlipTime_ >= halfFlipTime_;

  if (showFront_) {
    scale_ = (curFlipTime_.asSeconds() - halfFlipTime_.asSeconds()) / halfFlipTime_.asSeconds();
    scaleFactor_ = std::sin(scale_ * utils::getPi() / 2);
    spriteFront_.setScale(scaleFactor_, 1.0f);
  } else {
    scale_ = 1.f - curFlipTime_.asSeconds() / halfFlipTime_.asSeconds();
    scaleFactor_ = std::sin(scale_ * utils::getPi() / 2);
    spriteBack_.setScale(scaleFactor_, 1.0f);
  }
}

void Tile::draw(sf::RenderTarget &renderTarget, sf::RenderStates renderStates) const {
  if (showFront_) {
    renderTarget.draw(spriteFront_);
  } else {
    renderTarget.draw(spriteBack_);
  }
}