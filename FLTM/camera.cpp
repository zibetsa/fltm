#include "camera.h"
#include "utilities.h"

Camera::Camera() :
  updateShake_(false),
  shakeTimeLeft_(0.0f),
  shakeRadius_(0.0f),
  shakeRandomAngle_(0.0f),
  shakeOffset_(sf::Vector2f(0.0f, 0.0f))
{
}

Camera::~Camera() {
}

const sf::View &Camera::getView() {
  return view_;
}

const sf::FloatRect &Camera::getRect() {
  return viewRect_;
}

const sf::Vector2f &Camera::getCenter() const {
  return view_.getCenter();
}

void Camera::setRect(const sf::FloatRect &rect) {
  viewRect_ = rect;
  cameraCenter_ = sf::Vector2f(viewRect_.left + (viewRect_.width / 2), viewRect_.top + (viewRect_.height / 2));
}

void Camera::updateViewPort(const sf::Vector2f windowSize) {
  const sf::FloatRect cameraRect = getRect();
  const float w = cameraRect.width / windowSize.x;
  const float h = cameraRect.height / windowSize.y;
  const float x = (1.0f - w) / 2;
  const float y = (1.0f - h) / 2;
  view_.setViewport(sf::FloatRect(x, y, w, h));
}

void Camera::reset(const sf::FloatRect &rect) {
  view_.reset(rect);
}

void Camera::shake(const ShakeType shakeType) {
  switch (shakeType) {

    case ShakeType::WRONG_ANSWER:
      {
        shakeTimeLeft_ = 2.5f;
        shakeRadius_ = 15.0f;
      }
      break;

    default:
      {
        return;
      }
      break;

  }

  shakeRandomAngle_ = static_cast<float>(std::rand() % 360);
  shakeOffset_ = sf::Vector2f(std::sin(shakeRandomAngle_) * shakeRadius_, std::cos(shakeRandomAngle_) * shakeRadius_);
  view_.setCenter(sf::Vector2f(view_.getViewport().left, view_.getViewport().top) + shakeOffset_);
  updateShake_ = true;
}

void Camera::stopShaking() {
  updateShake_ = false;
  view_.setCenter(cameraCenter_);
}

void Camera::update(sf::Time dt) {
  if (updateShake_) {
    shakeTimeLeft_ -= dt.asSeconds();
    shakeRadius_ *= 0.9f;
    shakeRandomAngle_ += (150 + std::rand() % 60);
    shakeOffset_ = sf::Vector2f(std::sin(shakeRandomAngle_) * shakeRadius_, std::cos(shakeRandomAngle_) * shakeRadius_);
    if (shakeTimeLeft_ <= 0.0f) {
      updateShake_ = false;
      shakeTimeLeft_ = 0.0f;
    }
    view_.setCenter(cameraCenter_ + shakeOffset_);
  } else {
    view_.setCenter(cameraCenter_);
  }
}