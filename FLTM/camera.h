#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <SFML/Graphics.hpp>

class Camera {
 public:
  enum class ShakeType {
    WRONG_ANSWER,

    NUMBER_OF_SHAKE_TYPES
  };

  Camera();
  virtual ~Camera();

  const sf::View &getView();
  const sf::FloatRect &getRect();
  const sf::Vector2f &getCenter() const;

  void setRect(const sf::FloatRect &rect);
  void updateViewPort(const sf::Vector2f windowSize);
  void reset(const sf::FloatRect &rect);

  void shake(const ShakeType shakeType);
  void stopShaking();
  void update(sf::Time dt);

 private:
  sf::View view_;
  sf::FloatRect viewRect_;
  sf::Vector2f cameraCenter_;

  bool updateShake_;
  float shakeTimeLeft_;
  float shakeRadius_;
  float shakeRandomAngle_;
  sf::Vector2f shakeOffset_;

};

#endif